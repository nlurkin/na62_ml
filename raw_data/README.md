# Data directory

## Data format
  * v1: Original format with everything in a single csv file that had to be read line by line
  * v2: New format with one csv per dataframe. Each csv can be directly dumped into a dataframe

## Subdirs
  * gtkreco: extract of GTK hits for ML reco test
  * mc_kmu2_overlay: extracted from overlaid Kmu2 MC events
    * v1: Data format v1. No selection, just dump all tracks and vertices
  * mc_pnn_overlay: extracted from overlaid PNN MC events.
    * v1: Data format v1. No selection, just dump of all tracks and vertices
    * v2: Data format v2. Adding PreSelection and BestTrackSelection
    * v3: Data format v2. Adding original GTK-Vertex matching for K3pi MC and data
  * mc_k3pi_overlay:
    * v3: Data format v2. Adding original GTK-Vertex matching for K3pi MC and data
