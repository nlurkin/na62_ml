#!/usr/bin/env python
# encoding: utf-8
'''
shortdesc

train_model is a description

@author:     Nicolas Lurkin

@contact:    nicolas.lurkin@cern.ch
'''

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
from functools import partial
import os
import sys

#from na62_ml import wrapper
from na62_ml import pl_wrapper as wrapper
from na62_ml.overloads import CrossEntropyTrainer, EventSampler, NA62Metrics
import torch

__all__ = []
__version__ = 0.1
__date__ = '2021-02-19'
__updated__ = '2021-02-19'


def validate_args(args):
    if args.resume_epoch != "last" and args.resume_from is None:
        raise Exception("--resume-epoch is set, so --resume-from is required")


def run(args):
    torch.multiprocessing.set_sharing_strategy('file_system')

    data_path = args.data
    data_limit = args.data_limit
    num_workers = args.num_workers

    print("-------")
    print("Preparing network training")
    print(f"  Model taken from {args.config}")

    #data = wrapper.load_disktsdata(
    #    data_path, data_limit=data_limit, num_workers=num_workers, batch_sampler=EventSampler)
    data = wrapper.load_disktsdata(data_path, num_workers=num_workers, data_limit=data_limit) # Batch sampler?
    create_model = partial(wrapper.create_network_from_file, file = args.config)
    if args.resume_from is None:
        #t, model = wrapper.create_from_scratch(partial(wrapper.create_network_from_file, file=args.config),
        #                                       CrossEntropyTrainer, data, max_epochs=args.nepochs)
        t, model = wrapper.create_from_scratch(create_model, max_epochs=args.nepochs, log_dir=args.logdir)
    else:
        #t, model = wrapper.load_from_checkpoint(partial(wrapper.create_network_from_file, file=args.config),
        #                                        args.logdir, args.resume_from, CrossEntropyTrainer,
        #                                        data, args.resume_epoch, new_epochs=args.nepochs)
        t, model = wrapper.load_from_checkpoint(create_model, log_dir=args.logdir, version=args.resume_from,
                                                epoch=args.resume_epoch, new_epochs=args.nepochs)

    print("-------")
    print("Starting training")
    print(f"  Data located at {args.data}")
    print()
    _ = data.create_normalization_transform()
    #t.check_norm()
    data.set_use_norm(True)
    wrapper.train(t, model, data)

def run_jobs(script, args):
    os.system(script + " " + " ".join(args))

def main(argv = None):  # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = f"v{__version__}"
    program_build_date = str(__updated__)
    program_version_message = f"{program_name} {program_version} ({program_build_date})"
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    help_desc = f"{program_version_message}\n{program_shortdesc}"

    # Setup argument parser
    parser = ArgumentParser(description = help_desc, formatter_class = RawDescriptionHelpFormatter)
    parser.add_argument("--data", help = "Path to the on-disk data", required = True)
    parser.add_argument("--nepochs", type = int, help = "Number of epochs to run", required = True)
    parser.add_argument("--config", help = "Path to config file", required = True)
    parser.add_argument("--logdir", default = "runs", help = "Path to logdir [default: %(default)s]")

    parser.add_argument("--resume-from", type = int, help = "Model iteration to resume from")
    parser.add_argument("--resume-epoch", default = "last", help = "Epoch from which to resume from (requires --resume-from) [default: %(default)s]")
    parser.add_argument("--data-limit", type = int, help = "Maximum size of training sample")
    parser.add_argument("--num-workers", type = int, default = 0, help = "Number of workers for DataLoader [default: %(default)i]")

    parser.add_argument("--pre", help = "Runs a pre-job script")
    parser.add_argument("--pre-args", nargs="*", default=[], help = "Runs a pre-job script")
    parser.add_argument("--post", help = "Runs a post-job script")
    parser.add_argument("--post-args", nargs="*", default=[], help = "Runs a post-job script")

    # Process arguments
    args = parser.parse_args()

    validate_args(args)

    if args.pre is not None:
        run_jobs(args.pre, args.pre_args)

    run(args)

    if args.post is not None:
        run_jobs(args.post, args.post_args)

    return 0


if __name__ == "__main__":
    sys.exit(main())
