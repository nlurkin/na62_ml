from torch import nn
import torch

from na62_ml.models import DrawableNet


class NetBlock(DrawableNet):

    def __init__(self, input_size, width, output_size, nlayers = None, leakage = 0.1):
        super(NetBlock, self).__init__()
        self.layers = []

        for iLayer in range(nlayers - 1):
            layer = nn.Linear(input_size, width)
            self.layers.append(layer)
            self.add_module(f"mLayer_{iLayer}", layer)
            input_size = width
        layer = nn.Linear(input_size, output_size)
        self.layers.append(layer)
        self.add_module(f"mOutput_{iLayer}", layer)
        self.leaky = nn.LeakyReLU(leakage)

    def forward(self, x):
        for iLayer, layer in enumerate(self.layers):
            x = layer(x)
            if iLayer != len(self.layers) - 1:
                x = self.leaky(x)
        return x


class VariableWidthNet(DrawableNet):

    def __init__(self, layers, input_size = 18, leakage = 0.1):
        super(VariableWidthNet, self).__init__()
        self.layers = []
        for iLayer, layer_size in enumerate(layers):
            layer = nn.Linear(input_size, layer_size)
            self.layers.append(layer)
            self.add_module(f"iLayer_{iLayer}", layer)
            input_size = layer_size

        self.leaky = nn.LeakyReLU(leakage)

    def forward(self, x):
        for iLayer, layer in enumerate(self.layers):
            x = layer(x)
            if iLayer != len(self.layers) - 1:
                x = self.leaky(x)
        return x


class Net(DrawableNet):

    def __init__(self, leakage = 0.1):
        super(Net, self).__init__()
        input_size = 37
        self.subnet = VariableWidthNet([50, 50, 10],
                                       input_size = input_size)
        self.output = nn.Linear(10, 2)

        self.leaky = nn.LeakyReLU(leakage)

    def forward(self, x):
        x = self.subnet.forward(x)
        x = self.output(x)
        return x

model = None
optimizer = None
loss_fn = None


def get_used_vars():
    return ()


def get_model():
    global model
    if model is None:
        model = Net()
    return model


def get_loss_fn():
    global loss_fn
    if loss_fn is None:
        loss_fn = nn.CrossEntropyLoss()
    return loss_fn


def get_optimizer():
    global optimizer
    if optimizer is None:
        optimizer = torch.optim.SGD(get_model().parameters(), lr = 0.001)
    return optimizer


def get_name():
    return "Net_50_50_10"
