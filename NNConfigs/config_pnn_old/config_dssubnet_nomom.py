from torch import nn
import torch

from na62_ml.models import DrawableNet


class NetBlock(DrawableNet):

    def __init__(self, input_size, width, output_size, nlayers = None, leakage = 0.1, transform = None):
        super(NetBlock, self).__init__()
        self.layers = []

        for iLayer in range(nlayers - 1):
            layer = nn.Linear(input_size, width)
            self.layers.append(layer)
            self.add_module(f"mLayer_{iLayer}", layer)
            input_size = width
        layer = nn.Linear(input_size, output_size)
        self.layers.append(layer)
        self.add_module(f"mOutput_{iLayer}", layer)
        self.leaky = nn.LeakyReLU(leakage)
        self.transform = transform

    def forward(self, x):
        if self.transform is not None:
            x = self.transform(x)
        for iLayer, layer in enumerate(self.layers):
            x = layer(x)
            if iLayer != len(self.layers) - 1:
                x = self.leaky(x)
        return x


def get_ds_features(x):
    return x[:, get_used_features()]  # 35 for time


model = None
optimizer = None
loss_fn = None


def get_used_features():
    return (3, 4, 11, 12, 13, 15, 16, 17)  # 35 for time, 5 and 14 for momentum


def get_model():
    global model
    if model is None:
        model = NetBlock(input_size = len(get_used_features()),
                         width = 20,
                         nlayers = 3,
                         output_size = 2,
                         transform = get_ds_features)
    return model


def get_loss_fn():
    global loss_fn
    if loss_fn is None:
        loss_fn = nn.CrossEntropyLoss()
    return loss_fn


def get_optimizer():
    global optimizer
    if optimizer is None:
        optimizer = torch.optim.SGD(get_model().parameters(), lr = 0.001)
    return optimizer


def get_name():
    return "Net_NetBlockDS_i11h20h20h20o2_nomom"
