from torch import nn
import torch

from na62_ml.models import DrawableNet
import config_dssubnet_nomom
import config_gtksubnet


class NetBlock(DrawableNet):

    def __init__(self, input_size, width, output_size, nlayers = None, leakage = 0.1):
        super(NetBlock, self).__init__()
        self.layers = []

        for iLayer in range(nlayers - 1):
            layer = nn.Linear(input_size, width)
            self.layers.append(layer)
            self.add_module(f"mLayer_{iLayer}", layer)
            input_size = width
        layer = nn.Linear(input_size, output_size)
        self.layers.append(layer)
        self.add_module(f"mOutput_{iLayer}", layer)
        self.leaky = nn.LeakyReLU(leakage)

    def forward(self, x):
        for iLayer, layer in enumerate(self.layers):
            x = layer(x)
            if iLayer != len(self.layers) - 1:
                x = self.leaky(x)
        return x


class VariableWidthNet(DrawableNet):

    def __init__(self, layers, input_size = 18, leakage = 0.1):
        super(VariableWidthNet, self).__init__()
        self.layers = []
        for iLayer, layer_size in enumerate(layers):
            layer = nn.Linear(input_size, layer_size)
            self.layers.append(layer)
            self.add_module(f"iLayer_{iLayer}", layer)
            input_size = layer_size

        self.leaky = nn.LeakyReLU(leakage)

    def forward(self, x):
        for iLayer, layer in enumerate(self.layers):
            x = layer(x)
            if iLayer != len(self.layers) - 1:
                x = self.leaky(x)
        return x


class Net(DrawableNet):

    def __init__(self, leakage = 0.1):
        super(Net, self).__init__()
        input_size = 36
        self.ds_subnet = config_dssubnet_nomom.get_model()
        self.gtk_subnet = config_gtksubnet.get_model()
        #self.ds_subnet = NetBlock(input_size = ds_input_size,
        #                           width = 20,
        #                           nlayers = 3,
        #                           output_size = ds_output_size)
        self.subnet = VariableWidthNet([50, 50, 10],
                                       input_size = input_size + 2 + 2)
        self.output = nn.Linear(10, 2)

        self.leaky = nn.LeakyReLU(leakage)

    def forward(self, x):
        ds = self.leaky(self.ds_subnet(x))
        gtk = self.leaky(self.gtk_subnet(x))
        x = get_ds_features(x)
        subnet_input = torch.cat([x, ds, gtk], 1)
        # print(subnet_input.shape)
        x = self.subnet.forward(subnet_input)
        x = self.output(x)
        return x

model = None
optimizer = None
loss_fn = None


def get_ds_features(x):
    return x[:, get_used_features()]  # 35 for time

def get_used_features():
    a = list(range(38))
    del a[14]
    del a[5]
    return a  # 35 for time


def get_model():
    global model
    if model is None:
        model = Net()
        checkpoint_ds = torch.load("tsboard/v3/mc_pnn_overlay/Net_NetBlockDS_i11h20h20h20o2_nomom_0/Net_NetBlockDS_i11h20h20h20o2_nomom_0_epoch50.pth")
        model.ds_subnet.load_state_dict(checkpoint_ds['model_state_dict'])
        checkpoint_gtk = torch.load("tsboard/v3/mc_pnn_overlay/Net_NetBlockGTK_i16h30h30h30o2_0/Net_NetBlockGTK_i16h30h30h30o2_0_epoch100.pth")
        model.gtk_subnet.load_state_dict(checkpoint_gtk['model_state_dict'])
    return model


def get_loss_fn():
    global loss_fn
    if loss_fn is None:
        loss_fn = nn.CrossEntropyLoss()
    return loss_fn


def get_optimizer():
    global optimizer
    if optimizer is None:
        optimizer = torch.optim.SGD(get_model().parameters(), lr = 0.001)  # , momentum = 0.1, weight_decay = 1e-3)
    return optimizer


def get_name():
    return "Net_NetBlockDS_i11h20h20h20o2_VWN_50_50_10_NoMom"
