from torch import nn
import torch

from na62_ml.models import NetBlock


def get_ds_features(x):
    return x[:, (2, 3, 8, 9, 15, 16)]


model = None
optimizer = None
loss_fn = None
def get_model():
    global model
    if model is None:
        model = NetBlock(input_size = 6,
                         width = 20,
                         nlayers = 3,
                         output_size = 2,
                         transform = get_ds_features)
    return model


def get_loss_fn():
    global loss_fn
    if loss_fn is None:
        loss_fn = nn.CrossEntropyLoss()
    return loss_fn


def get_optimizer():
    global optimizer
    if optimizer is None:
        optimizer = torch.optim.SGD(get_model().parameters(), lr = 0.001)
    return optimizer


def get_name():
    return "Net_NetBlockDS_20x3"
