from torch import nn
import torch

from na62_ml.models import DrawableNet, NetBlock


class VariableWidthNet(DrawableNet):

    def __init__(self, layers, input_size = 18, leakage = 0.1):
        super(VariableWidthNet, self).__init__()
        self.layers = []
        for iLayer, layer_size in enumerate(layers):
            layer = nn.Linear(input_size, layer_size)
            self.layers.append(layer)
            self.add_module(f"iLayer_{iLayer}", layer)
            input_size = layer_size

        self.leaky = nn.LeakyReLU(leakage)

    def forward(self, x):
        for iLayer, layer in enumerate(self.layers):
            x = layer(x)
            if iLayer != len(self.layers) - 1:
                x = self.leaky(x)
        return x


def get_ds_features(x):
    return x[:, (2, 3, 8, 9, 15, 16)]


class Net(DrawableNet):

    def __init__(self, leakage = 0.1):
        super(Net, self).__init__()
        input_size = 17
        ds_input_size = 6
        ds_output_size = 2
        self.ds_subnet = NetBlock(input_size = ds_input_size,
                                   width = 20,
                                   nlayers = 3,
                                   output_size = ds_output_size)
        self.subnet = VariableWidthNet([50, 50, 10],
                                       input_size = input_size + ds_output_size)
        self.output = nn.Linear(10, 2)

        self.leaky = nn.LeakyReLU(leakage)

    def forward(self, x):
        # print(x.shape)
        ds = self.leaky(self.ds_subnet(get_ds_features(x)))
        x = x[:,:-1]
        subnet_input = torch.cat([x, ds], 1)
        # print(subnet_input.shape)
        x = self.subnet.forward(subnet_input)
        x = self.output(x)
        return x

model = None
optimizer = None
loss_fn = None
def get_model():
    global model
    if model is None:
        model = Net()
        checkpoint = torch.load("runs/Net_NetBlockDS_20x3_0/Net_NetBlockDS_20x3_0_epoch100.pth")
        model.ds_subnet.load_state_dict(checkpoint['model_state_dict'])
    return model


def get_loss_fn():
    global loss_fn
    if loss_fn is None:
        loss_fn = nn.CrossEntropyLoss()
    return loss_fn


def get_optimizer():
    global optimizer
    if optimizer is None:
        optimizer = torch.optim.SGD(get_model().parameters(), lr = 0.001)
    return optimizer


def get_name():
    return "Net_NetBlockDS_20x3_VWN_50_50_10_NoTime"
