from torch import nn
import torch

from na62_ml.models import NetBlock


def get_gtk_features(x):
    return x[:, (0, 1, 5, 6, 13, 14)]

model = None
optimizer = None
loss_fn = None
def get_model():
    global model
    if model is None:
        model = NetBlock(input_size = 6,
                         width = 20,
                         nlayers = 3,
                         output_size = 2,
                         transform = get_gtk_features)
    return model


def get_loss_fn():
    global loss_fn
    if loss_fn is None:
        loss_fn = nn.CrossEntropyLoss()
    return loss_fn


def get_optimizer():
    global optimizer
    if optimizer is None:
        optimizer = torch.optim.SGD(get_model().parameters(), lr = 0.001)
    return optimizer


def get_name():
    return "Net_NetBlock_20x3"
