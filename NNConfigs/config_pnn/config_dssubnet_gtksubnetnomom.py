from torch import nn
import torch

import config_dssubnet_nomom
import config_gtksubnet
from na62_ml.models import DrawableNet, VariableWidthNet


class Net(DrawableNet):

    def __init__(self, leakage = 0.1):
        super(Net, self).__init__()
        input_size = 36
        self.ds_subnet = config_dssubnet_nomom.get_model()
        self.gtk_subnet = config_gtksubnet.get_model()
        #self.ds_subnet = NetBlock(input_size = ds_input_size,
        #                           width = 20,
        #                           nlayers = 3,
        #                           output_size = ds_output_size)
        self.subnet = VariableWidthNet(layers = [50, 50, 10],
                                       input_size = input_size + 2 + 2,
                                       output_size = 2)
        self.leaky = nn.LeakyReLU(leakage)

    def forward(self, x):
        ds = self.leaky(self.ds_subnet(x))
        gtk = self.leaky(self.gtk_subnet(x))
        x = get_ds_features(x)
        subnet_input = torch.cat([x, ds, gtk], 1)
        # print(subnet_input.shape)
        x = self.subnet.forward(subnet_input)
        return x

model = None
optimizer = None
loss_fn = None


def get_ds_features(x):
    return x[:, get_used_features()]  # 35 for time

def get_used_features():
    a = list(range(38))
    del a[14]
    del a[5]
    return a  # 35 for time


def get_model():
    global model
    if model is None:
        model = Net()
        checkpoint_ds = torch.load("tsboard/v3/mc_pnn_overlay/DSNoMom/Net_NetBlockDS_i11h20h20h20o2_nomom_9/Net_NetBlockDS_i11h20h20h20o2_nomom_9_epoch100.pth")
        model.ds_subnet.load_state_dict(checkpoint_ds['model_state_dict'])
        checkpoint_gtk = torch.load("tsboard/v3/mc_pnn_overlay/GTK/Net_NetBlockGTK_i16h30h30h30o2_1/Net_NetBlockGTK_i16h30h30h30o2_1_epoch100.pth")
        model.gtk_subnet.load_state_dict(checkpoint_gtk['model_state_dict'])
    return model


def get_loss_fn():
    global loss_fn
    if loss_fn is None:
        loss_fn = nn.CrossEntropyLoss()
    return loss_fn


def get_optimizer():
    global optimizer
    if optimizer is None:
        optimizer = torch.optim.SGD(get_model().parameters(), lr = 0.001)  # , momentum = 0.1, weight_decay = 1e-3)
    return optimizer


def get_name():
    return "Net_NetBlockDS_i11h20h20h20o2_VWN_50_50_10_NoMom"
