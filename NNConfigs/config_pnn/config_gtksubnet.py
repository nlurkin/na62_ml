from torch import nn
import torch

from na62_ml.models import NetBlock


def get_gtk_features(x):
    return x[:, get_used_features()]


model = None
optimizer = None
loss_fn = None


def get_used_features():
    return (0, 1, 2, 7, 8, 9, 10, 18, 19, 20, 21, 22, 23, 24, 25, 26)  # 34 for time


def get_model():
    global model
    if model is None:
        model = NetBlock(input_size = len(get_used_features()),
                         width = 30,
                         nHiddenLayers = 3,
                         output_size = 2,
                         transform = get_gtk_features)
    return model


def get_loss_fn():
    global loss_fn
    if loss_fn is None:
        loss_fn = nn.CrossEntropyLoss()
    return loss_fn


def get_optimizer():
    global optimizer
    if optimizer is None:
        optimizer = torch.optim.SGD(get_model().parameters(), lr = 0.001)
    return optimizer


def get_name():
    return "Net_NetBlockGTK_i16h30h30h30o2"
