from torch import nn
import torch

from na62_ml.models import DrawableNet, VariableWidthNet
from na62_ml.pl_lib import LightningClassifier


class Net(DrawableNet):

    def __init__(self, leakage = 0.1):
        super(Net, self).__init__()
        input_size = 38
        self.subnet = VariableWidthNet(layers_size=[50, 50, 10],
                                       input_size = input_size, output_size=2)

        self.leaky = nn.LeakyReLU(leakage)

    def forward(self, x):
        x = self.subnet.forward(x)
        return x

model = None
optimizer = None
loss_fn = None


def get_used_vars():
    return ()


def get_model():
    global model
    if model is None:
        model = Net()
    return model


def get_loss_fn():
    global loss_fn
    if loss_fn is None:
        loss_fn = nn.CrossEntropyLoss()
    return loss_fn


def get_optimizer():
    global optimizer
    if optimizer is None:
        optimizer = torch.optim.SGD(get_model().parameters(), lr = 0.001)
    return optimizer


def get_name():
    return "Net_50_50_10"


class SimpleModel(LightningClassifier):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        input_size = 38
        self.subnet = VariableWidthNet(layers_size = [50, 50, 10],
                                       input_size = input_size,
                                       output_size = 2)
        self.loss_fn = nn.CrossEntropyLoss()
        self.example_input_array = torch.Tensor([1]*input_size)

    def forward(self, x):
        x = self.subnet.forward(x)
        return x

    def configure_optimizers(self):
        optimizer = torch.optim.SGD(self.parameters(), lr = 0.001)
        return optimizer

    @staticmethod
    def get_name():
        return "Net_NetBlockDS_i11h20h20h20o2_VWN_50_50_10_NoTime"

model = None

def pl_get_model():
    global model
    if model is None:
        model = SimpleModel()
    return model
