from na62_ml import pl_lib
from na62_ml import models
import torch

from na62_ml.overloads import NA62Metrics

class SimpleModel(pl_lib.LightningClassifier):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        input_size = 36
        self.subnet = models.VariableWidthNet(layers_size = [530, 200],
                                       input_size = input_size,
                                       output_size = 2)
        self.loss_fn = torch.nn.CrossEntropyLoss()
        self.example_input_array = torch.Tensor([1]*input_size)

    def forward(self, x):
        x = self.subnet.forward(x)
        return x

    def configure_optimizers(self):
        optimizer = torch.optim.SGD(self.parameters(), lr = 0.0001, momentum=0.9, weight_decay=1e-3)
        return optimizer

    def get_name(self):
        return "Joel"


model = None

def pl_get_model():
    global model
    if model is None:
        model = SimpleModel()
    return model

def config_metrics():
    NA62Metrics.index_cols = ["Run", "Burst", "Event", "GTKTrackIndex"]
    NA62Metrics.group_by = ["Run", "Burst", "Event"]
