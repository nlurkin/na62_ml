'''
Created on 25 Jan 2021

@author: nlurkin
'''
from functools import partial

from captum.insights import AttributionVisualizer, Batch
from captum.insights.attr_vis.features import GeneralFeature
import torch

from na62_ml import wrapper
from na62_ml.overloads import CrossEntropyTrainer


def formatted_data_iter(dataloader):
    dlit = iter(dataloader)
    while True:
        d, l, _ = next(dlit)
        yield Batch(inputs = d, labels = l)


def main():

    dlt, dlv, _ = wrapper.load_disktsdata("data/mc_pnn_overlay/v3/MCMatch/", num_workers = 4)
    create_model = partial(wrapper.create_network_from_file, file = "NNConfigs/config_pnn/config_dssubnet_gtksubnet.py")

    t = wrapper.load_from_checkpoint(create_model, "tsboard/v3/mc_pnn_overlay/All/", 9, CrossEntropyTrainer, dlt, dlv)
    t.create_normalization_transform()
    t.check_norm()

    col_names = dlv.dataset.get_columns_names()

    visualizer = AttributionVisualizer(
        models = [t.model],
        score_func = lambda o: torch.nn.functional.softmax(o, 1),
        classes = ["False", "True"],
        features = GeneralFeature(categories = col_names, name = "Vertex"),
        dataset = formatted_data_iter(t.dataloader),
    )
    visualizer.serve(debug = True, port = 10001)


if __name__ == "__main__":
    main()
