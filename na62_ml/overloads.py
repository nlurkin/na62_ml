'''
Created on 26 Jan 2021

@author: nlurkin
'''

import sklearn.metrics
import torch
from tqdm.autonotebook import tqdm

import matplotlib.pyplot as plt
from na62_ml.ml_lib import PandasDataSet, Trainer, InputError, efficiency_with_error
import numpy as np
import pandas as pd
import warnings


def sum_dict(d1, d2):
    for k in d1:
        if isinstance(d1[k], dict):
            sum_dict(d1[k], d2[k])
        else:
            d1[k] += d2[k]


def frac_it(v1, v2):
    rat = np.nan if v2 == 0 else v1 / v2
    return f"{v1}/{v2} ({rat:.2%})"


def compute_metrics(testevt, model):
    labels = testevt[1]
    output = model(testevt[0])
    output_decision = torch.max(output, dim=1)[1]
    output_proba = torch.softmax(output, dim=1)[:,1]
    meta = testevt[2]
    indices = pd.DataFrame()  # testevt[2].to_frame().reset_index(drop = True)

    indices["Good"] = labels.cpu()
    indices["Output"] = output_decision.cpu()
    indices["OutputProba"] = output_proba.detach().cpu()
    indices["Vertices"] = 1
    indices["Correct"] = indices["Good"] == indices["Output"]
    indices["Incorrect"] = indices["Good"] != indices["Output"]
    indices["Correct_assoc"] = indices["Correct"] & indices["Good"]
    indices["Incorrect_assoc"] = indices["Output"] & indices["Incorrect"]
    indices["Missed_assoc"] = indices["Good"] & ~indices["Output"]
    indices.index = meta.index

    return indices

def match_indices_levels(ref, df):
    nref = 1
    if isinstance(ref, pd.DataFrame) or isinstance(ref, pd.Series):
        nref = ref.index.nlevels
    elif isinstance(ref, list):
        nref = len(ref)
    levels_to_drop = list(range(-1, nref-df.index.nlevels-1, -1))
    return df.index.droplevel(levels_to_drop)


def display_metrics_pnn(indices, print_it = False, group_index="Event"):
    sums = indices
    sums_with_assoc = sums.loc[sums["Good"] > 0]

    # # vertices
    nvtx = len(sums)
    nvtx_exp_assoc = sums["Good"].sum()
    nvtx_assoc = sums["Output"].sum()
    nvtx_corr_assoc = sums_with_assoc["Correct_assoc"].sum()
    nvtx_incorr_assoc = sums["Incorrect_assoc"].sum()

    # # Events
    nevts = len(sums.groupby(group_index).first())
    nevts_exp_assoc = len(sums_with_assoc.groupby(group_index).first())
    nevts_assoc = len(sums.loc[sums["Output"] > 0].groupby(group_index).first())
    nassoc_per_event = sums["Output"].groupby(group_index).sum()
    if nassoc_per_event.index.nlevels>1:
        sums_index = match_indices_levels(nassoc_per_event, sums)
    else:
      sums_index = sums.index.get_level_values(0)

    events_single = sums.loc[sums_index.isin(nassoc_per_event.loc[nassoc_per_event == 1].index)]
    nevts_single_assoc = len(events_single.groupby(group_index).first())
    nevts_single_correct = len(events_single.loc[events_single["Correct_assoc"]])
    nevts_single_incorrect = len(events_single.loc[events_single["Incorrect_assoc"]])

    vtx_true_positive = efficiency_with_error(nvtx_corr_assoc, nvtx_assoc)
    vtx_false_positive = efficiency_with_error(nvtx_incorr_assoc, nvtx_assoc)
    vtx_efficiency = efficiency_with_error(nvtx_corr_assoc, nvtx_exp_assoc)
    evt_assoc_rate = efficiency_with_error(nevts_assoc, nevts_exp_assoc)
    evt_single_rate = efficiency_with_error(nevts_single_assoc, nevts_exp_assoc)
    evt_true_positive = efficiency_with_error(nevts_single_correct, nevts_single_assoc)
    evt_false_positive = efficiency_with_error(nevts_single_incorrect, nevts_single_assoc)

    if print_it:
        print("=== Vertex level values ===")
        print(f"# Tested:           {nvtx:>10}")
        print(f"# Expected assoc.:  {nvtx_exp_assoc:>10}")
        print(f"# Assoc. found   :  {nvtx_assoc:>10}")
        print(f"# Correct assoc.:   {nvtx_corr_assoc:>10}")
        print(f"# Incorrect assoc.: {nvtx_incorr_assoc:>10}")
        print()

        print("=== Event level values ===")
        print(f"# Events:                  {nevts:>10}")
        print(f"# Expected assoc.:         {nevts_exp_assoc:>10}")
        print(f"# Assoc. found:            {nevts_assoc:>10}")
        print(f"# Single assoc.:           {nevts_single_assoc:>10}")
        print(f"# Single correct assoc.:   {nevts_single_correct:>10}")
        print(f"# Single Incorrect assoc.: {nevts_single_incorrect:>10}")
        print()

        print("=== Summary ===")
        print(f"Vtx association rate       : {frac_it(nvtx_assoc, nvtx_exp_assoc)}")
        print(f"Vtx correct rate           : {frac_it(nvtx_corr_assoc, nvtx_assoc)}")
        print(f"Vtx efficiency             : {frac_it(nvtx_corr_assoc, nvtx_exp_assoc)}")
        print(f"Vtx mismatch rate          : {frac_it(nvtx_incorr_assoc, nvtx_assoc)}")
        print(f"Evt Association rate       : {frac_it(nevts_assoc, nevts_exp_assoc)}")
        print(f"Evt Single Association rate: {frac_it(nevts_single_assoc, nevts_exp_assoc)}")
        print(f"Evt Mismatch rate          : {frac_it(nevts_single_incorrect, nevts_single_assoc)}")
    return {"Vertex": {"Total": nvtx, "Expected": nvtx_exp_assoc, "Efficiency": vtx_efficiency, "Correct": vtx_true_positive, "Incorrect": vtx_false_positive},
            "Event": {"Total": nevts, "Expected": nevts_exp_assoc, "Assoc": evt_assoc_rate, "Single": evt_single_rate,
            "Correct": evt_true_positive, "Incorrect": evt_false_positive}}


def display_metrics(indices, print_it = False):
    # sums = indices.groupby("Event").sum()
    sums = indices
    sums_with_assoc = sums.loc[sums["Good"] > 0]

    tested = sums["Vertices"].sum()
    exp_assoc = sums["Good"].sum()
    no_assoc = sums["Vertices"] - sums["Output"]
    assoc = sums["Output"].sum()
    assoc_with_assoc = sums_with_assoc["Output"].sum()
    correct_assoc = sums["Correct_assoc"]
    incorrect_assoc = sums["Output"] - sums["Correct_assoc"]
    missed_assoc = sums["Missed_assoc"]
    events = len(sums)
    correct_events = sums["Vertices"] == sums["Correct"]
    events_with_assoc = len(sums_with_assoc)
    correct_events_with_assoc = sums_with_assoc["Vertices"] == sums_with_assoc["Correct"]
    partial_events_with_assoc = (sums_with_assoc["Incorrect_assoc"] == 0) & (sums_with_assoc["Correct_assoc"] < sums_with_assoc["Good"]) & (sums_with_assoc["Correct_assoc"] > 0)
    noassoc_events_with_assoc = sums_with_assoc["Output"] == 0
    mispartial_events_with_assoc = (sums_with_assoc["Correct_assoc"] > 0) & (sums_with_assoc["Incorrect_assoc"] > 0)
    mis_events_with_assoc = (sums_with_assoc["Correct_assoc"] == 0) & (sums_with_assoc["Incorrect_assoc"] > 0)

    any_matching_probability = assoc.sum() / tested if tested > 0 else np.nan
    correct_match_probability = correct_assoc.sum() / assoc if assoc > 0 else np.nan
    incorrect_match_probability = incorrect_assoc.sum() / assoc if assoc > 0 else np.nan
    missing_probability = missed_assoc.sum() / exp_assoc.sum()

    matching_probability = sums_with_assoc["Output"].sum() / sums_with_assoc["Good"].sum()
    mismatch_probability = sums_with_assoc["Incorrect_assoc"].sum() / assoc_with_assoc if assoc_with_assoc > 0 else np.nan

    if print_it:
        print("== Raw events ==")
        print(f"# Tested vertices           {tested.sum():>10}")
        print(f"# Expected associations     {exp_assoc.sum():>10}")
        print(f"# without association       {no_assoc.sum():>10}")
        print(f"# of associations           {assoc.sum():>10}")
        print(f"# of correct associations   {correct_assoc.sum():>10}")
        print(f"# of incorrect associations {incorrect_assoc.sum():>10}")
        print(f"# of missed associations    {missed_assoc.sum():>10}")
        print(f"# Events                    {events:>10}")
        print(f"# Events fully correct      {correct_events.sum():>10}")
        print("== Events with vertex ==")
        print(f"# Events with association         {events_with_assoc:>10}")
        print(f"# Events with correct association {correct_events_with_assoc.sum():>10}")
        print(f"# Events with partial association {partial_events_with_assoc.sum():>10}")
        print(f"# Events with no association      {noassoc_events_with_assoc.sum():>10}")
        print(f"# Events with partial mismatch    {mispartial_events_with_assoc.sum():>10}")
        print(f"# Events with complete mismatch   {mis_events_with_assoc.sum():>10}")

        print("== Probabilities ==")
        print(f"Any matching      {any_matching_probability:>8.2%}")
        print(f"Correct matching  {correct_match_probability:>8.2%}")
        print(f"Incorrect matching{incorrect_match_probability:>8.2%}")
        print(f"Matching ineff.   {missing_probability:>8.2%}")
        print(f"Matching          {matching_probability:>8.2%}")
        print(f"Mismatch          {mismatch_probability:>8.2%}")

    return {"Vertex": {"any_matching_probability": any_matching_probability,
                       "correct_match_probability": correct_match_probability,
                       "incorrect_match_probability": incorrect_match_probability,
                       },
            "Event": {"missing_probability": missing_probability,
                      "matching_probability": matching_probability,
                      "mismatch_probability": mismatch_probability,
                      }
            }


def compute_metrics_single_event(testevt, model, print_it = False):
    labels = testevt[1]

    # Output of model taking all vertices where model is positive
    output = torch.max(model(testevt[0]), 1)[1]

    # Output of model taking only vertex with maximum positiveness
    max_vtx = torch.max(model(testevt[0]), 0)[1][1]
    output_max = torch.zeros(labels.shape, dtype = torch.int64)
    output_max[max_vtx] = 1
    output_max = output_max & output  # Make sure we discard the vertex if it is predicted as negative

    good = labels == 1
    has_output = output == 1
    correct = labels == output
    incorrect = labels != output
    correct_assoc = correct & good
    incorrec_assoc = has_output & incorrect

    correct_max = labels == output_max
    incorrect_max = labels != output_max
    correct_assoc_max = correct_max & good
    incorrec_assoc_max = (output_max == 1) & incorrect_max

    # # vertices
    nvtx = labels.shape[0]
    nvtx_exp_assoc = int(good.sum())
    nvtx_assoc = int(has_output.sum())
    nvtx_corr_assoc = int(correct_assoc.sum())
    nvtx_incorr_assoc = int(incorrec_assoc.sum())

    nvtx_corr_assoc_max = correct_assoc_max.sum()
    nvtx_incorr_assoc_max = incorrec_assoc_max.sum()

    # # Events
    nevts = 1
    nevts_exp_assoc = int(nvtx_exp_assoc > 0)
    nevts_assoc = int(nvtx_assoc > 0)
    event_single = nvtx_assoc == 1
    nevts_single_assoc = int(event_single)
    nevts_single_correct = int(event_single and nvtx_corr_assoc == 1)
    nevts_single_incorrect = int(event_single and nvtx_incorr_assoc == 1)

    event_single_max = (output_max == 1).sum() == 1
    nevts_single_correct_max = int(event_single_max and nvtx_corr_assoc_max == 1)
    nevts_single_incorrect_max = int(event_single_max and nvtx_incorr_assoc_max == 1)

    return {"Vertex": {"Total": nvtx, "Expected": nvtx_exp_assoc, "Assoc": nvtx_assoc, "Correct": nvtx_corr_assoc,
                       "Incorrect": nvtx_incorr_assoc},
            "Event": {"Total": nevts, "Expected": nevts_exp_assoc, "Assoc": nevts_assoc, "Single": nevts_single_assoc,
                      "Correct": nevts_single_correct, "Incorrect": nevts_single_incorrect,
                      # Single_Max === nevts_assoc
                      "Correct_Max": nevts_single_correct_max, "Incorrect_Max": nevts_single_incorrect_max,
            }}


def display_metrics_single_event(metrics):
    nvtx_corr_assoc = metrics["Vertex"]["Correct"]
    nvtx_assoc = metrics["Vertex"]["Assoc"]
    nvtx_incorr_assoc = metrics["Vertex"]["Incorrect"]

    nevts_assoc = metrics["Event"]["Assoc"]
    nevts_exp_assoc = metrics["Event"]["Expected"]
    nevts_single_assoc = metrics["Event"]["Single"]
    nevts_single_correct = metrics["Event"]["Correct"]
    nevts_single_incorrect = metrics["Event"]["Incorrect"]

    metrics["Vertex"]["Correct"] = efficiency_with_error(nvtx_corr_assoc, nvtx_assoc)
    metrics["Vertex"]["Incorrect"] = efficiency_with_error(nvtx_incorr_assoc, nvtx_assoc)
    metrics["Event"]["Assoc"] = efficiency_with_error(nevts_assoc, nevts_exp_assoc)
    metrics["Event"]["Single"] = efficiency_with_error(nevts_single_assoc, nevts_exp_assoc)
    metrics["Event"]["Correct"] = efficiency_with_error(nevts_single_correct, nevts_single_assoc)
    metrics["Event"]["Incorrect"] = efficiency_with_error(nevts_single_incorrect, nevts_single_assoc)

    return metrics

def pnn_metrics(indices, fill_which_match, group_index = "Event"):
    if type(group_index)!=str and len(group_index)>1:
      myIndices = match_indices_levels(group_index, indices)
      n_k3pi = len(myIndices.unique())
    else:
      n_k3pi = len(indices.index.get_level_values(0).unique())
    n_good_per_event = indices.loc[indices["Good"]==1].groupby(group_index).count()
    n_exp_match = len(n_good_per_event.loc[n_good_per_event["Good"]>0])
    indices = fill_which_match(indices, group_index=group_index)
    matched_events = indices.loc[indices["Selected"]]
    n_match = len(matched_events)
    n_wrong_match = len(matched_events.loc[matched_events["Incorrect_assoc"]])

    p_match = n_match/n_k3pi if n_k3pi!=0 else np.nan
    p_mistag_k = n_wrong_match/n_match if n_match!=0 else np.nan


    return {"PNN": {"NK3pi": n_k3pi, "Expected_matches": n_exp_match, "Matches": n_match, "P(match)": p_match,
                   "Wrong_matches": n_wrong_match, "P(mistag,K)": p_mistag_k}}


def select_best(indices, level=0.3, group_index = "Event"):
    max_proba = indices.groupby(group_index)["OutputProba"].max()
    indices["ProbaDiff"] = np.abs(indices["OutputProba"] - max_proba)
    select = (indices["Output"]==1) & (indices["ProbaDiff"]<level)
    indices["NMatches"] = indices.loc[select].groupby(group_index)["Output"].count()
    indices["Selected"] = select & (indices["NMatches"]==1)
    return indices

def select_unique(indices, group_index = "Event"):
    index_names = indices.index.names
    if type(group_index)==str:
      rm_indices = len(index_names)-1
    else:
      rm_indices = len(index_names)-len(group_index)
    values = indices.loc[indices["Output"]==1].groupby(group_index)["Output"].count()
    indices = indices.reset_index().set_index(index_names[:-rm_indices])
    indices["NMatches"] = values
    indices["NMatches"] = indices["NMatches"].fillna(0).astype(int)
    indices = indices.reset_index().set_index(index_names)
    indices["Selected"] = (indices["Output"]==1) & (indices["NMatches"]==1)
    return indices

def generate_combined_df(pl_module, model):
  cols = list(pl_module.val_dataloader().dataset.get_columns_names())
  df = pd.DataFrame(columns=cols)
  all_meta = []
  all_preds = []
  all_labels = []

  pl_module.set_use_norm(True)
  ds = pl_module.val_dataloader().dataset
  it = iter(ds)
  it_norm = iter(ds)
  for i in tqdm(range(len(ds))):
      ds.use_normalization(False)
      ds.use_transform(False)
      data_in = next(it)[0]
      ds.use_normalization(True)
      ds.use_transform(True)
      data_in_norm, labels, meta = next(it_norm)
      pred = torch.argmax(model(data_in_norm)).item()
      meta = ds.getmeta(meta[0], meta[1]).drop("sample_id")
      df.loc[meta["index"]] = data_in.numpy()
      all_meta.append(pd.DataFrame(meta, dtype=float).T)
      all_preds.append(pred)
      all_labels.append(labels.item())
  all_meta = pd.concat(all_meta).astype({"index": int, "Run": int, "Burst": int, "Event": int}).set_index("index")
  df = df.merge(all_meta, left_index=True, right_index=True)
  df["Pred"] = all_preds
  df["Label"] = all_labels
  return df, cols

def compute_intensity_dependence(df, group_index = "Event"):
  ## Correctmatch in Joel DS == Good in both DS
  df["GoodMatch"] = (df["Label"]==df["Pred"])
  df["BadMatch"] = (df["Label"]!=df["Pred"])
  df["Incorrect_assoc"] = df["BadMatch"] & (df["Pred"]==1)
  x = df.groupby(group_index).aggregate({"Pred": "sum", "Label":"count", "lambdaGTK": "first", "Incorrect_assoc": "max"})
  x["Mistag"] = (x["Pred"]==1) & (x["Incorrect_assoc"])
  x = x.reset_index().groupby(pd.cut(x.reset_index()["lambdaGTK"], np.arange(-1, 1400, 50))).aggregate({"Label": "count", "Pred": lambda x: sum(x==1), "Mistag": "sum"})
  p_match = (x["Pred"]/x["Label"]).dropna()
  dp_match = (np.sqrt((np.sqrt(x["Pred"])/x["Pred"])**2 + (np.sqrt(x["Label"])/x["Label"])**2)*p_match).loc[p_match.index].fillna(0)
  p_mistag = (x["Mistag"]/x["Pred"]).dropna()
  dp_mistag = (np.sqrt((np.sqrt(x["Mistag"])/x["Mistag"])**2 + (np.sqrt(x["Pred"])/x["Pred"])**2)*p_mistag).loc[p_mistag.index].fillna(0)
  plt.errorbar([_.mid for _ in x.index if _ in p_match.index], p_match, yerr=dp_match, label="P(match)")
  plt.errorbar([_.mid for _ in x.index if _ in p_mistag.index], p_mistag, yerr=dp_mistag, label="P(mistag_K)")
  plt.ylim(0, 1)
  plt.legend()

  return p_match, dp_match, p_mistag, dp_mistag

class CrossEntropyTrainer(Trainer):

    def __init__(self, *args, **kwargs):
        super(CrossEntropyTrainer, self).__init__(*args, **kwargs)
        self.validate_losses = []
        self.train_accuracy = []
        self.validate_accuracy = []

    def prepare_ax(self):
        _, ax = plt.subplots(2, 1)
        return ax[1], ax

    def show_user_stats(self, ax):
        ax[0].plot(self.validate_accuracy, label = "Validation")
        ax[0].plot(self.train_accuracy, label = "Training")
        ax[0].set_title("Accuracy - Loss")
        ax[0].legend()
        ax[0].grid("both")
        ax[0].get_xaxis().set_ticklabels([])
        ax[1].plot(self.validate_losses, label = "Validation loss")
        ax[1].grid("both")
        ax[1].legend()
        ax[1].set_xlabel("Epoch")

    def compute_roc_curve(self, dataset = "validate"):
        warnings.warn(
            "CrossEntropyTrainer.compute_roc_curve is deprecated, use NA62Metrics.compute_roc_curve instead",
            DeprecationWarning
        )
        if dataset == "validate":
            dataset = self.dataloader_val
        elif dataset == "train":
            dataset = self.dataloader
        elif not isinstance(dataset, torch.utils.data.DataLoader):
            raise InputError("dataset expects either 'validate', 'train',"
                             "or a dataloader inheriting from torch.utils.data.DataLoader")
        outputs = []
        labels = []
        for data in tqdm(dataset, desc = "Extracting", leave = False):
            d, l = data[0].to(self.device), data[1]
            out = torch.sigmoid(self.model(d))
            outputs.append(out.detach().cpu())
            labels.append(l)

        outputs = torch.cat(outputs, dim = 0)
        labels = torch.cat(labels, dim = 0)
        fpr, tpr, thr = sklearn.metrics.roc_curve(labels, outputs[:, 1])
        roc_auc = sklearn.metrics.auc(fpr, tpr)

        _, ax = plt.subplots(1, 2)
        ax[0].set_title('Receiver Operating Characteristic')
        ax[0].plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
        ax[0].legend(loc = 'lower right')
        ax[0].plot([0, 1], [0, 1], 'r--')
        ax[0].set_xlim([0, 1])
        ax[0].set_ylim([0, 1])
        ax[0].set_ylabel('True Positive Rate')
        ax[0].set_xlabel('False Positive Rate')

        ax[1].set_title("Probabilities")
        ax[1].hist([outputs[labels == 0, 1], outputs[labels == 1, 1]], bins = 20, range = (0, 1),
                   density = True, histtype = "step", label = ["Fake vertex", "True vertex"])
        ax[1].legend()
        ax[1].set_xlim([0, 1])
        ax[1].set_ylabel("A.U.")
        ax[1].set_xlabel("Predicted probability (True vertex)")

        return fpr, tpr, thr

    def get_meta(self, dl, subids):
        warnings.warn(
            "CrossEntropyTrainer.get_meta is deprecated, use NA62Metrics.get_meta instead",
            DeprecationWarning
        )

        metas = [dl.dataset.getmeta(*_.numpy()) for _ in subids]
        return pd.concat(metas, axis = 1).T.set_index(["Event", "GTKIndex", "DSIndex"])

    def compute_validation_scalars(self, do_report = True, replace_var = None, assume_full_event = False):
        warnings.warn(
            "CrossEntropyTrainer.compute_validation_scalars is deprecated, use NA62Metrics.compute_validation_scalars instead",
            DeprecationWarning
        )

        correct = 0
        total = 0
        val_loss = 0.0
        val_steps = 0
        merged_df = None
        for data in tqdm(self.dataloader_val, desc = "Validating", leave = False):
            d, l, subid = data[0].to(self.device), data[1].to(self.device), data[2]
            subid = torch.stack(subid, axis = 1)
            if replace_var:
                d = self.replace_variable(d, replace_var)
            outputs = self.model(d)

            # self.train_accuracy.append((torch.max(outputs, 1).indices==l).sum().item()/len(l))
            # for softmax output
            _, predicted = torch.max(outputs, 1)
            correct += (predicted == l).sum().item()
            # correct = 0
            total += l.size(0)
            loss = self.criterion(outputs, l)
            val_loss += loss.cpu().numpy()
            val_steps += 1
            meta = self.get_meta(self.dataloader_val, subid)
            if assume_full_event:
                for evtID in set(meta.index.get_level_values(0)):
                    evt_indices = np.where(meta.index.get_loc_level(evtID, 0)[0])
                    id1 = compute_metrics_single_event(testevt = (d[evt_indices], l[evt_indices]), model = self.model)
                    if merged_df is None:
                        merged_df = id1
                    else:
                        sum_dict(merged_df, id1)
            else:
                id1 = compute_metrics(testevt = (d, l, meta), model = self.model)
                if merged_df is None:
                    merged_df = id1
                else:
                    merged_df = pd.concat([merged_df, id1])

        self.validate_accuracy.append(efficiency_with_error(correct, total))
        self.validate_losses.append(val_loss / val_steps)
        local_metrics = {"train": {"losses": np.nan if not do_report else self.training_losses[-1], },
                         "validate": {"accuracy": self.validate_accuracy[-1],
                                      "losses": self.validate_losses[-1],
                                      }
                         }
        if assume_full_event:
            metrics_dict = display_metrics_single_event(merged_df)
        else:
            metrics_dict = display_metrics_pnn(merged_df)
        local_metrics.update(metrics_dict)
        if hasattr(self.dataloader.dataset, "sample_id") and self.dataloader.dataset.sample_id is not None:
            local_metrics["sample_id"] = self.dataloader.dataset.sample_id
        if self.reporter is not None and do_report:
            report_step = self.prev_report_step + len(self.dataloader.dataset)
            self.reporter.epoch_end(global_epoch = self.global_epoch, global_step = report_step,
                                   metrics_dic = local_metrics)
            self.prev_report_step = report_step

        return local_metrics, merged_df

class EventSampler(torch.utils.data.Sampler):

    def __init__(self, dataset, min_batch_size = 16, col_remap=None, group_by=None, shuffle=True):
        inputdir = dataset.inputdir
        self.events_map = None
        self.min_batch_size = min_batch_size
        self.nsamples = 0
        self.len = None

        if col_remap is None:
            col_remap = {"DSIndex": "NSamples"}
        if group_by is None:
            group_by = ["Event"]

        events_cols = group_by + ["NSamples"]

        for min_sample, partition in tqdm(sorted(dataset.partition.items(), key = lambda x: x[0])):
            if partition is not None:
                meta = pd.read_parquet(f"{inputdir}/meta/metadata_{partition}.pqet")
                events = meta.rename(columns = col_remap)[events_cols]

                if self.events_map is None:
                    self.events_map = events
                else:
                    self.events_map = self.events_map.truncate(after = self.events_map.iloc[min_sample - 1].name)
                    self.events_map = pd.concat([self.events_map, events], ignore_index = True)
            else:
                self.events_map = self.events_map.truncate(after = self.events_map.iloc[min_sample - 1].name)

        self.nsamples = len(self.events_map)
        self.events_map = self.events_map.reset_index().rename(columns={"index": "SampleIndex"})
        self.events_map = self.events_map.groupby(group_by, sort = False).agg({"SampleIndex": list, "NSamples": "count"})
        self.events_map = self.events_map.values
        if shuffle:
            self.events_sampler = torch.utils.data.RandomSampler(range(self.events_map.shape[0]))
        else:
            self.events_sampler = range(self.events_map.shape[0])
        self.events_sampler_iter = None
        ## No iterator created. Will be different every time we create the EventSampler iterator

    def __get_sequence(self, reset=False):
        # Create the new iterator if does not exist or asked for a reset
        if self.events_sampler_iter is None or reset:
            self.events_sampler_iter = list(iter(self.events_sampler))
            # We have to invalidate the previous length
            self.len = None
        return self.events_sampler_iter

    def __len__(self):
        # If len is defined, we can send it again (no new iterator since then)
        if self.len is not None:
            return self.len

        ret_list = []
        total_len = 0
        for eventID in self.__get_sequence():
            event_ref = self.events_map[eventID]
            ret_list.extend(event_ref[0])
            if len(ret_list) < self.min_batch_size:
                continue
            total_len += 1
            ret_list = []

        if len(ret_list) > 0:
            total_len += 1

        self.len = total_len
        return self.len
        #return int(self.nsamples / self.min_batch_size)

    def __iter__(self):
        iterator = self.__get_sequence()
        ret_list = []
        for eventID in iterator:
            event_ref = self.events_map[eventID]
            ret_list.extend(event_ref[0])
            if len(ret_list) < self.min_batch_size:
                continue
            yield ret_list
            ret_list = []

        if len(ret_list) > 0:
            yield ret_list

        ## Now invalidate the seqeunce
        self.len = None
        self.events_sampler_iter = None


class RegressionTrainer(Trainer):

    def __init__(self, *args, **kwargs):
        super(RegressionTrainer, self).__init__(*args, **kwargs)
        self.validate_losses = []

    def log_rmse(self, features, labels):
        # To further stabilize the value when the logarithm is taken, set the
        # value less than 1 as 1
        labels = labels.reshape([-1, 1])
        clipped_preds = torch.clamp(self.model(features), 1, float('inf'))
        rmse = torch.sqrt(self.criterion(torch.log(clipped_preds),
                               torch.log(labels)))
        return rmse.item()

    def compute_validation_scalars(self, do_report = True, replace_var = None, assume_full_event = False):
        data, labels = self.dataloader_val.dataset[:]
        val_loss = self.criterion(self.model(data), labels.reshape([-1, 1])).cpu().numpy()

        # Compute validation losses
        self.validate_losses.append(val_loss)
        local_metrics = {"train": {"losses": np.nan if not do_report else self.training_losses[-1], },
                         "validate": {
                                      "losses": self.validate_losses[-1],
                                      "log_rmse": self.log_rmse(*self.dataloader_val.dataset[:])
                                      }
                         }
        if hasattr(self.dataloader.dataset, "sample_id") and self.dataloader.dataset.sample_id is not None:
            local_metrics["sample_id"] = self.dataloader.dataset.sample_id
        if self.reporter is not None and do_report:
            report_step = self.prev_report_step + len(self.dataloader.dataset)
            self.reporter.epoch_end(global_epoch = self.global_epoch, global_step = report_step,
                                   metrics_dic = local_metrics)
            self.prev_report_step = report_step

        return local_metrics


####### Below are the overloads for the pytorch-lightning
from na62_ml.pl_lib import NA62DataModule, DictLogger
import pytorch_lightning as pl
from na62_ml.ml_lib import FileError, DiskTensorDataset
import os

class GTKDSDataModule(NA62DataModule):
    def __init__(self, data_dir, data_limit = None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.data_dir = data_dir
        self.use_test = False
        self.data_limit = data_limit

    def prepare_data(self):
        # Check that all input directories exist
        if not os.path.exists(self.data_dir):
            raise FileError(f"Directory does not exist {self.data_dir}")
        if not os.path.exists(f"{self.data_dir}/train"):
            raise FileError(f"train directory not found in {self.data_dir}")
        if not os.path.exists(f"{self.data_dir}/validate"):
            raise FileError(f"train directory not found in {self.data_dir}")
        if os.path.exists(f"{self.data_dir}/test"):
            self.use_test = True

    def train_dataloader(self):
        if self.dl_train is None:
            # Create the dataloader
            train_set = DiskTensorDataset(f"{self.data_dir}/train", size_limit = self.data_limit)

            self.dl_train = self.prepare_dl(train_set)

        self.check_apply_norm(self.dl_train)
        return self.dl_train


    def val_dataloader(self):
        if self.dl_validate is None:
            validate_set = DiskTensorDataset(f"{self.data_dir}/validate",
                                             size_limit = None if self.data_limit is None else int(self.data_limit / 10))


            self.dl_validate = self.prepare_dl(validate_set, shuffle=False)

        self.check_apply_norm(self.dl_validate)
        return self.dl_validate

    def test_dataloader(self):
        if not self.use_test:
            return None

        if self.dl_test is None:
            test_set = DiskTensorDataset(f"{self.data_dir}/test",
                                         size_limit = None if self.data_limit is None else int(self.data_limit / 10))

            self.dl_test = self.prepare_dl(test_set, shuffle=False)

        self.check_apply_norm(self.dl_test)
        return self.dl_test

class SimplePandasDataSet(NA62DataModule):
    def __init__(self, dataframe, targetColumn):
        super().__init__()
        self.df = dataframe
        self.targetColumn = targetColumn

    def prepare_data(self):
        # Create datasets
        self.train_set = PandasDataSet(self.df, self.targetColumn)
        self.validate_set = PandasDataSet(self.df, self.targetColumn)

    def train_dataloader(self):
        # Create DataLoaders
        if self.dl_train is None:
            self.dl_train = self.prepare_dl(self.train_set)
        self.check_apply_norm(self.dl_train)

        return self.dl_train

    def val_dataloader(self):
        # Create DataLoaders
        if self.dl_validate is None:
            self.dl_validate = self.prepare_dl(self.validate_set)
        self.check_apply_norm(self.dl_validate)
        return self.dl_validate

class NA62Metrics(object):
    index_cols = ["Event", "GTKIndex", "DSIndex"]
    group_by = "Event"

    @staticmethod
    def _get_dataset(data, dataset):
        if isinstance(data, pl.LightningDataModule):
            if dataset == "validate":
                dataset = data.val_dataloader()
            elif dataset == "train":
                dataset = data.train_dataloader()
            elif dataset == "test":
                dataset = data.test_dataloader()
            else:
                raise InputError("dataset expects either 'validate', 'train', 'test'")
        elif isinstance(data, torch.utils.data.DataLoader):
            dataset = data
        else:
            raise InputError("data expects either a pl.LightningDataModule, "
                             "or a dataloader inheriting from torch.utils.data.DataLoader")
        return dataset

    @staticmethod
    def _get_full_predictions(model, dataset, raw=False):
        outputs = []
        labels = []
        for data in tqdm(dataset, desc = "Extracting", leave = False):
            d, l = data[0].to(model.device), data[1]
            out = torch.sigmoid(model(d))
            outputs.append(out.detach().cpu())
            labels.append(l)

        outputs = torch.cat(outputs, dim = 0)
        labels = torch.cat(labels, dim = 0)

        return outputs, labels


    @staticmethod
    def compute_cut_accuracy(model, data, dataset="validate"):
        dataset = NA62Metrics._get_dataset(data, dataset)
        outputs, labels = NA62Metrics._get_full_predictions(model, dataset)

        out_true_vtx = outputs.detach().numpy()[labels==1]
        total = len(out_true_vtx)
        match_vertex = []# Tagged as match, and is a vertex -> Correct
        unmatch_loose = [] # Not tagged as match, and is not a vertex -> Incorrect
        test_val = np.arange(0, 1, 0.01)
        for i in test_val:
            match_vertex.append(sum(out_true_vtx[:,1]>i)/total)
            unmatch_loose.append(sum(out_true_vtx[:,0]>i)/total)

        fig = plt.figure()
        plt.plot(test_val, match_vertex, label="Match")
        plt.plot(test_val, unmatch_loose, label="Mismatch")
        ax = plt.gca()
        ax.set_yticks(np.arange(0, 1, 0.02), minor=True)
        ax.set_yticks(np.arange(0, 1, 0.02), minor=True)
        plt.grid(which="minor", alpha=0.2, axis="both")
        plt.grid(which="major", alpha=0.5, axis="both")
        plt.xlabel("Cut value")
        plt.ylabel("Fraction")
        plt.legend()

        return fig

    @staticmethod
    def compute_roc_curve(model, data, dataset = "validate"):
        dataset_type = dataset
        dataset = NA62Metrics._get_dataset(data, dataset)
        outputs, labels = NA62Metrics._get_full_predictions(model, dataset)

        fpr, tpr, thr = sklearn.metrics.roc_curve(labels, outputs[:, 1])
        roc_auc = sklearn.metrics.auc(fpr, tpr)

        prec, recall, prthr = sklearn.metrics.precision_recall_curve(labels, outputs[:, 1])

        figures = {}
        fig = plt.figure()
        plt.title('Precision Recall')
        plt.plot(recall, prec, "b", label = f"AP = {np.mean(prec):0.2f}")
        plt.legend(loc = 'lower right')
        plt.ylabel('Precision')
        plt.xlabel('Recall')
        figures["PR curve"] = fig

        fig = plt.figure()
        plt.title('Receiver Operating Characteristic')
        plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
        plt.legend(loc = 'lower right')
        plt.plot([0, 1], [0, 1], 'r--')
        plt.xlim([0, 1])
        plt.ylim([0, 1])
        plt.ylabel('True Positive Rate')
        plt.xlabel('False Positive Rate')
        figures["Roc curve"] = fig

        fig = plt.figure()
        plt.title("Probabilities")
        plt.hist([outputs[labels == 0, 1], outputs[labels == 1, 1]], bins = 20, range = (0, 1),
                   density = True, histtype = "step", label = ["Fake vertex", "True vertex"])
        plt.legend()
        plt.xlim([0, 1])
        plt.ylabel("A.U.")
        plt.xlabel("Predicted probability (True vertex)")
        figures["Probabilities curve"] = fig

        figures["Cut Accuracy"] = NA62Metrics.compute_cut_accuracy(model, data, dataset_type)

        return figures, fpr, tpr, thr

    @staticmethod
    def get_meta(dl, subids):
        metas = [dl.dataset.getmeta(*_.numpy()) for _ in subids]
        return pd.concat(metas, axis = 1).T.set_index(NA62Metrics.index_cols)

    @staticmethod
    def compute_validation_scalars(datamodule, model, do_report = True, replace_var = None, assume_full_event = False):
        correct = 0
        total = 0
        merged_dict = None
        merged_base = None
        dataloader = datamodule.val_dataloader()
        for data in tqdm(dataloader, desc = "Validation scalars", leave = False):
            d, l, subid = data[0].to(model.device), data[1].to(model.device), data[2]
            subid = torch.stack(subid, axis = 1)
            #if replace_var:
            #    d = self.replace_variable(d, replace_var) --> Here
            outputs = model(d)

            # for softmax output
            _, predicted = torch.max(outputs, 1)
            correct += (predicted == l).sum().item()
            total += l.size(0)
            meta = NA62Metrics.get_meta(dataloader, subid)
            metrics_base = compute_metrics(testevt = (d, l, meta), model = model)
            if merged_base is None:
                merged_base = metrics_base
            else:
                merged_base = pd.concat([merged_base, metrics_base])

            if assume_full_event:
                meta["row_index"] = meta.reset_index().index
                for evtID in set(meta.index.droplevel(-1)):
                    evt_indices = meta.sort_index().loc[evtID]["row_index"].values
                    id1 = compute_metrics_single_event(testevt = (d[evt_indices], l[evt_indices]), model = model)
                    if merged_dict is None:
                        merged_dict = id1
                    else:
                        sum_dict(merged_dict, id1)

        validate_accuracy = efficiency_with_error(correct, total)

        local_metrics = {"val": {"accuracy": validate_accuracy.nominal_value}}

        if assume_full_event:
            metrics_dict = display_metrics_single_event(merged_dict)
        else:
            metrics_dict = display_metrics_pnn(merged_base, group_index=NA62Metrics.group_by)
        metrics_dict.update(pnn_metrics(merged_base, fill_which_match=select_unique, group_index=NA62Metrics.group_by))

        local_metrics.update(metrics_dict)
        if hasattr(dataloader.dataset, "sample_id") and dataloader.dataset.sample_id is not None:
            local_metrics["sample_id"] = dataloader.dataset.sample_id

        return local_metrics

class TrackMatchingScalars(DictLogger):
    def __init__(self, assume_full_event=False, **kwargs):
        super().__init__(**kwargs)
        self.assume_full_event = assume_full_event

    def on_validation_epoch_end(self, trainer, pl_module):
        if isinstance(trainer, pl.Trainer):
            # This is actually the trainer
            datamodule = trainer.datamodule
        else:
            datamodule = trainer
        metrics = NA62Metrics.compute_validation_scalars(datamodule, pl_module, assume_full_event=self.assume_full_event)
        self.report(metrics, pl_module)

class SummaryCurves(pl.Callback):
    def on_fit_end(self, trainer, pl_module):
        if isinstance(trainer, pl.Trainer):
            # This is actually the trainer
            datamodule = trainer.datamodule
        else:
            datamodule = trainer
        figures, _, _, _ = NA62Metrics.compute_roc_curve(pl_module, datamodule)

        for name, fig in figures.items():
            plt.close(fig)
            pl_module.logger.experiment.add_figure(name, fig, pl_module.global_step)
