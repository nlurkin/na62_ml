import os
import shutil
from pathlib import Path

import numpy as np
import pytorch_lightning as pl
import torch
import uncertainties
from pytorch_lightning.loggers import TensorBoardLogger
from tqdm.autonotebook import tqdm


class NA62DataModule(pl.LightningDataModule):
    def __init__(self, batch_size = 10, shuffle = True, drop_last = True, num_workers = 0, batch_sampler = None):
        super().__init__()
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.drop_last = drop_last
        self.num_workers = num_workers
        self.batch_sampler = batch_sampler
        self.norm = None
        self.use_norm = False
        self.dl_train = None
        self.dl_test = None
        self.dl_validate = None

        print("Using options\n  -", "\n  - ".join([str(_) for _ in self.get_options().items()]))

    def get_options(self):
        dl_options = {}
        if self.batch_sampler is not None:
            print("Using batch_sampler for dataloader ... disabling batch_size, shuffle and drop_last options")
            dl_options["batch_sampler"] = None
        else:
            dl_options["batch_size"] = self.batch_size
            dl_options["shuffle"] = self.shuffle
            dl_options["drop_last"] = self.drop_last
        dl_options["num_workers"] = self.num_workers

        return dl_options

    def extract_normalization_from(self, from_path=None, from_dataset=None):
        if from_path is not None:
            ds_norm = torch.load(from_path)
            if ds_norm is not None:
                print("Extracting saved normalization from provided path")
                self.norm = ds_norm
        elif from_dataset is not None:
            ds_norm = from_dataset.get_normalization()
            if ds_norm is not None:
                print("Extracting saved normalization from provided dataset")
                self.norm = ds_norm


    def extract_normalization(self):
        oldVal = self.use_norm
        self.use_norm = False # Avoid infinite loop
        dataloader = self.train_dataloader()
        ds_norm = dataloader.dataset.get_normalization()
        self.use_norm = oldVal
        if ds_norm is not None:
            print("Extracting saved normalization from dataset")
            self.norm = ds_norm
            return
        print("Extracting normalization .", end = "")
        input_size = next(iter(dataloader))[0].shape[1]
        mean = torch.zeros((input_size))
        std = torch.zeros((input_size))
        n = 0
        for d in tqdm(dataloader):
            d = d[0]
            ni = d.shape[0]
            mui = torch.mean(d, dim=0)

            n += ni
            mean += ni*mui

        print(".", end = "")
        mu = mean/n
        for d in tqdm(dataloader):
            d = d[0]
            ni = d.shape[0]
            mui = torch.mean(d, dim=0)
            sigmai = torch.std(d, dim=0)**2

            std += (ni - 1) * sigmai + ni * (mui - mu) ** 2

        sigma = np.sqrt(std / (n - 1))
        self.norm = (mu, sigma)
        print(". Done")
        self.save_normalization_for_ds()

    def save_normalization_for_ds(self):
        if self.norm is not None:
            torch.save(self.norm, Path(self.train_dataloader().dataset.inputdir) / "norm.pt")

    def create_normalization_transform(self, from_path=None, from_dataset=None):
        if self.norm is None:
            if from_path is None and from_dataset is None:
                self.extract_normalization()
            else:
                self.extract_normalization_from(from_path, from_dataset)

        return lambda data: (data - self.norm[0]) / self.norm[1]

    def set_use_norm(self, apply_norm = True):
        self.use_norm = self.norm is not None and apply_norm

    def check_apply_norm(self, dl):
        if self.use_norm:
            dl.dataset.set_normalization_transform(self.create_normalization_transform())
            dl.dataset.use_normalization(True)
        else:
            dl.dataset.use_normalization(False)

    def prepare_dl(self, dataset, **options):
        dl_options = self.get_options()
        if "batch_sampler" in dl_options:
            dl_options["batch_sampler"] = self.batch_sampler(dataset)
        dl_options.update(options)
        dl = torch.utils.data.DataLoader(dataset, **dl_options)

        return dl

class LightningClassifier(pl.LightningModule):
    def __init__(self):
        super().__init__()

    def loss(self, logits, y):
        loss = self.loss_fn(logits, y)
        return loss

    def training_step(self, data, batch_idx):
        d, l = data[0].to(self.device), data[1].to(self.device)

        # forward + backward + optimize
        outputs = self.forward(d)
        loss = self.loss_fn(outputs, l)
        self.log("train/loss", loss)
        return loss

    def validation_step(self, data, batch_idx):
        d, l = data[0].to(self.device), data[1].to(self.device)
        outputs = self.forward(d)

        loss = self.loss_fn(outputs, l)
        self.log("val/loss", loss)

    def save_model(self, path = None):
        # Unused in principle -> TODO remove (also bad idea according to lightning)
        if path is None:
            path = f"{self.logger.log_dir}/model_{self.get_name()}.pth"
        print(f"Saving model in {path}")
        torch.save(self.model, path)

class DictLogger(pl.Callback):
    def report(self, metrics_dic, model):
        for tag, dic in metrics_dic.items():
            if isinstance(dic, dict):
                for k, v in dic.items():
                    if isinstance(v, uncertainties.core.Variable):
                        v = v.nominal_value
                    if isinstance(v, str):
                        model.logger.experiment.add_text(f"{tag}/{k}", v, model.global_step)
                    else:
                        model.log(f"{tag}/{k}", v)
            elif isinstance(dic, str):
                model.logger.experiment.add_text(f"{tag}", dic, model.global_step)
            else:
                if isinstance(dic, uncertainties.core.Variable):
                    dic = dic.nominal_value
                model.log(tag, dic)

def configure_logger(set_name, run_name, force_new = False, resume_version=None):
    # The logger will save in a directory ./set_name/ -> Global save directory
    # Each run (=model) will be saved in a subdirectory ./set_name/run_name
    # Each version (=attempt) will be saved in ./set_name/run_name/version_n

    # force_new: deletes all previous versions of the current experiment

    # Delete existing runs?
    if force_new and os.path.exists(f"{set_name}/{run_name}"):
        shutil.rmtree(f"{set_name}/{run_name}")
    # Version is dealt with by TensorBoardLogger itself if resume_version is None (=will take the next available version)

    logger = TensorBoardLogger(set_name, name=run_name, log_graph=resume_version is not None, default_hp_metric=False, version=resume_version)

    # Add custom scalars -> train and validation lossed on same plot
    logger.experiment.add_custom_scalars({"Metrics":
                                          {"Loss": ["Multiline", ["train/loss", "val/loss"]]}
                                         })

    return logger

def find_checkpoint(set_name, run_name, version="last", epoch="last"):
    cp_dir = os.path.join(set_name, run_name)
    if version == "last":
        version = int(sorted(os.listdir(cp_dir),key=lambda v: int(v.split("_")[-1]))[-1].split("_")[-1])
    cp_dir += f"/version_{version}/checkpoints/"

    cpt = os.listdir(cp_dir)
    if epoch != "last":
        cpt = [_ for _ in cpt if int(_.split("-")[0].split("=")[1])<epoch]

    used = sorted(cpt, key = lambda v: int(v.split("-")[0].split("=")[1]))[-1]
    epoch = int(used.split("-")[0].split("=")[1])
    return cp_dir + used, version, epoch+1 # Because epoch start at 0

def configure_trainer(logger, max_epochs, from_checkpoint=None, callbacks=None):
    #early_stop_callback = EarlyStopping(monitor='val/loss', min_delta=0.00, patience=5, verbose=False, mode='min')

    callback_list = []
    if callbacks is not None:
        callback_list.extend(callbacks)
    #callback_list.append(early_stop_callback)

    #if with_tuning:
    #    callback_list.append(TuneReportCallback({"loss": "val/rmse", "acc": "val/rel_diff"}, on="validation_end"))

    trainer = pl.Trainer(max_epochs=max_epochs,
                         #progress_bar_refresh_rate=0,
                         #weights_summary=None,
                         callbacks=callback_list,
                         logger=logger, resume_from_checkpoint=from_checkpoint)

    return trainer
