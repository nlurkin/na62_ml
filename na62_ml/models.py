import torch.nn as nn
import matplotlib.pyplot as plt
import pytorch_lightning as pl


def activation_func(activation):
    return  nn.ModuleDict([
        ['relu', nn.ReLU(inplace = True)],
        ['leaky_relu0p1', nn.LeakyReLU(negative_slope = 0.1, inplace = True)],
        ['selu', nn.SELU(inplace = True)],
        ['none', nn.Identity()]
    ])[activation]

class DrawableNet(pl.LightningModule):
    '''
    Base class for my networks. Allows drawing a representation of
    the network with matplotlib.
    '''

    def __init__(self):
        super(DrawableNet, self).__init__()

    def vis_linear(self, layer, name):
        _, ax = plt.subplots(1, 2)
        plt.gcf().suptitle(name)
        d = layer.state_dict()["weight"].data.numpy()
        ax[0].imshow(d, origin = "lower")
        ax[0].set_title("Weights")
        ax[1].plot(layer.state_dict()["bias"].data.numpy())
        ax[1].set_title("Biases")

    def visualize_network(self):
        for name, m in self._modules.items():
            if isinstance(m, nn.Linear):
                self.vis_linear(m, name)
            elif isinstance(m, DrawableNet):
                m.visualize_network()
            elif getattr(m, "__module__", None) == nn.modules.activation.__name__:
                # Skip activation modules
                pass
            else:
                print("Unknown module: ", type(m))


class VariableWidthNet(DrawableNet):
    '''
    Network block with variabe width layers.
    Layers size, input size and output size are parameterized.
    A transform can be specified to convert incompatible inputs
    '''

    def __init__(self, input_size, output_size, layers_size, activation = "leaky_relu0p1", transform = None, moduleID = "VW"):
        super(VariableWidthNet, self).__init__()
        self.layers = nn.ModuleList()

        for isize, osize in zip(([input_size] + layers_size)[:-1], layers_size):
            self.layers.append(nn.Linear(isize,osize))
        self.layers.append(nn.Linear(layers_size[-1],output_size))

        # iLayer = 0
        # for iLayer, layer_size in enumerate(layers_size):
        #     layer = nn.Linear(input_size, layer_size)
        #     self.layers.append(layer)
        #     input_size = layer_size

        # layer = nn.Linear(input_size, output_size)
        # iLayer += 1
        # self.layers.append(layer)
        # self.add_module(f"{moduleID}Output_{iLayer}", layer)

        self.activation = activation_func(activation)
        self.transform = transform
        self.ml_len = len(self.layers)

    def forward(self, x):
        if self.transform is not None:
            x = self.transform(x)
        for iLayer, layer in enumerate(self.layers):
            x = layer(x)
            if iLayer != self.ml_len - 1:
                x = self.activation(x)
        return x


class NetBlock(VariableWidthNet):
    '''
    Network block with fixed width hidden layers.
    Depth, input size and output size are parameterized.
    A transform can be specified to convert incompatible inputs
    '''

    def __init__(self, input_size, output_size, width, nHiddenLayers = None, moduleID = "B", **kwargs):
        layers_size = [width] * nHiddenLayers
        super(NetBlock, self).__init__(input_size, output_size, layers_size, moduleID = moduleID, **kwargs)


class AutoEncoderNet(VariableWidthNet):

    def __init__(self, nfeatures, ncoded, moduleID = "A", **kwargs):
        super(AutoEncoderNet, self).__init__(nfeatures, nfeatures, [ncoded], moduleID = moduleID, **kwargs)
