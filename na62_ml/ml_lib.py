import math
import os
import re
import shutil

import torch
from torch.utils.tensorboard import SummaryWriter
from tqdm.autonotebook import tqdm

import numpy as np
import pandas as pd
from scipy.stats import binom
from uncertainties.core import ufloat
import uncertainties.core
from pathlib import Path


class FileError(Exception):
    pass


class ConfigError(Exception):
    pass


class InputError(Exception):
    pass


class TrainingError(Exception):
    pass


# TODO: - catch infinite gradients and prevent going further
#       - normalize inputs to prevent infinite gradients
#       - Look at pytorch lightning
def to_int(val):
    try:
        val = int(val)
    except ValueError:
        pass
    return val


def efficiency_with_error(npass, ntot):
    if ntot == 0:
        p = np.nan
        err = np.nan
    else:
        p = npass / ntot
        err = binom.std(ntot, p) / ntot
    return ufloat(p, err)

def save_compatibility_check_files(data, model, compat_path="compat"):
    traced_script_module = model.to_torchscript()
    torch.jit.save(traced_script_module, f"{compat_path}/model.pt")
    data.set_use_norm(False)
    dsample = next(iter(data.val_dataloader()))
    pd.DataFrame(dsample[0]).to_csv(f"{compat_path}/input_raw.csv")
    pd.DataFrame(dsample[1]).to_csv(f"{compat_path}/labels.csv")
    data.create_normalization_transform()
    pd.DataFrame(data.norm[0]).T.to_csv(f"{compat_path}/mu.csv")
    pd.DataFrame(data.norm[1]).T.to_csv(f"{compat_path}/sigma.csv")
    data.set_use_norm(True)
    dsample = next(iter(data.val_dataloader()))
    o = model(dsample[0])
    pd.DataFrame(dsample[0]).to_csv(f"{compat_path}/input_norm.csv")
    pd.DataFrame(o.detach().numpy()).to_csv(f"{compat_path}/output.csv")


class MyBaseDataset(torch.utils.data.Dataset):
    def __init__(self):
        self.norm_transform = None
        self.with_norm = False
        self.with_transform = False
        self.transform_list = []

    def check_apply_norm_transform(self, data):
        if self.with_norm and self.norm_transform is not None:
            data = self.norm_transform(data)

        if self.with_transform:
            for transform in self.transform_list:
                data = transform(data)

        return data

    def set_normalization_transform(self, transform):
        self.norm_transform = transform

    def use_normalization(self, useit: bool):
        self.with_norm = useit

    def use_transform(self, useit: bool):
        self.with_transform = useit

    def add_transform(self, transform):
        self.transform_list.append(transform)


class DiskTensorDataset(MyBaseDataset):

    def __init__(self, inputdir, size_limit = None):
        super().__init__()
        self.inputdir = Path(inputdir)
        self.partition = {}
        self.tot_entries = 0
        self.meta_cache = {}
        self.sample_id = None

        for file in os.listdir(f"{inputdir}/sets"):
            if file.split(".")[-1] != "pt":
                continue
            nevts = torch.load(f"{inputdir}/sets/{file}").shape[0]
            self.partition[self.tot_entries] = re.search("data_(\S+)\.pt", file).group(1)
            self.tot_entries += nevts
            if size_limit is not None and self.tot_entries > size_limit:
                self.tot_entries = size_limit
                break
        self.partition[self.tot_entries] = None
        self.checkout_sample_id()

    def __getitem__(self, i):
        entries_list = sorted(self.partition.keys())
        if i < 0 or i > self.__len__():
            raise IndexError("Index out of range")

        partition_index = np.searchsorted(entries_list, i, side = "right") - 1
        partition = self.partition[entries_list[partition_index]]
        sub_index = i - entries_list[partition_index]
        dataset = torch.load(f"{self.inputdir}/sets/data_{partition}.pt")
        labels = torch.load(f"{self.inputdir}/labels/labels_{partition}.pt")
        data = self.check_apply_norm_transform(dataset[sub_index]).float()
        return data, self.get_label(labels, sub_index), (partition_index, sub_index)

    def get_label(self, labels, sub_index):
        return labels[sub_index].long()

    def checkout_sample_id(self):
        if self.sample_id is not None:
            return
        meta = self.getmeta(0, 0)
        if "sample_id" in meta:
            self.sample_id = meta["sample_id"]
            print("Using original sample_id", self.sample_id)
        else:
            # Temporary: extract info from input dir
            sample_type = self.inputdir.parts[1].split("_")
            sample_subtype = None
            sample_rev = None
            sample_extract = self.inputdir.parts[-3]
            sample_process = self.inputdir.parts[-2]
            if sample_type[0]=="mc":
                sample_subtype = sample_type[1]
                sample_type = "mc"
                sample_rev = "v214"
            elif sample_type[0] == "data":
                sample_subtype = self.inputdir.parts[2]
                sample_rev = "v204"
                sample_type = "data"
            self.sample_id = f"{sample_type}_{sample_subtype}_{sample_rev}_{sample_extract}_{sample_process}"

    def getmeta(self, partition_index, sub_index):
        entries_list = sorted(self.partition.keys())
        partition = self.partition[entries_list[partition_index]]
        if not partition in self.meta_cache:
            meta = pd.read_parquet(f"{self.inputdir}/meta/metadata_{partition}.pqet")
            self.meta_cache[partition] = meta

        return self.meta_cache[partition].iloc[sub_index]

    def get_normalization(self):
        p = Path(self.inputdir) / "norm.pt"
        if p.exists():
            return torch.load(str(p))
        return None

    def get_columns_names(self):
        if not os.path.exists(f"{self.inputdir}/../columns.csv"):
            raise FileError(f"File {self.inputdir}/../columns.csv does not exist")
        return np.loadtxt(f"{self.inputdir}/../columns.csv", dtype = "str")[1:]

    def __len__(self):
        return sorted(self.partition.keys())[-1]

    def build_pandas(self, max_load, labels_names=None):
        max_load = min(max_load, len(self))-1
        max_index = np.searchsorted(sorted(self.partition.keys()), max_load, side="right")

        df_ds = pd.DataFrame()
        df_labels = pd.DataFrame()
        df_meta = pd.DataFrame()
        for index in range(max_index):
            partition = self.partition[index]
            dataset = pd.DataFrame(torch.load(f"{self.inputdir}/sets/data_{partition}.pt"), columns=self.get_columns_names())
            labels = pd.DataFrame(torch.load(f"{self.inputdir}/labels/labels_{partition}.pt"), columns=labels_names)
            meta = pd.read_parquet(f"{self.inputdir}/meta/metadata_{partition}.pqet").set_index("index")
            dataset.index = meta.index
            labels.index = meta.index
            df_ds = pd.concat([df_ds, dataset])
            df_labels = pd.concat([df_labels, labels])
            df_meta = pd.concat([df_meta, meta])

        return df_ds.merge(df_labels, left_index=True, right_index=True).merge(df_meta, left_index=True, right_index=True)


class PandasDataSet(MyBaseDataset):
    def __init__(self, dataframe, label_columns):
        super().__init__()
        self.numeric_only = False
        self.target = torch.Tensor(dataframe[label_columns].values).long()
        dataframe = dataframe.drop(columns = label_columns)

        numeric, categories = [], []
        for col in dataframe.dtypes.items():
            # uint8 is used typically for dummies
            if str(col[1]) not in ["object", "category", "uint8"]:
                numeric.append(col[0])
            elif str(col[1]) == "category":
                categories.append(col[0])
                dataframe[col[0]] = dataframe[col[0]].cat.codes
            elif str(col[1]) == "uint8":
                categories.append(col[0])

        self.numeric_features = torch.Tensor(dataframe[numeric].values)
        self.category_features = torch.Tensor(dataframe[categories].values)

    def __getitem__(self, i):
        data = self.check_apply_norm_transform(self.numeric_features[i])
        label = self.target[i]

        if self.numeric_only:
            return data, label
        else:
            return torch.cat([data, self.category_features[i]], dim=-1), label

    def __len__(self):
        return len(self.target)

    def get_normalization(self):
        return self.numeric_features.mean(dim=0), self.numeric_features.std(dim=0)

class DataLoader():
    def __init__(self, data, target, batch_size=4, shuffle=True, labels_dtype="float"):
        self.index = data.index
        self.data = np.array(data)
        self.target = np.array(target)
        self.batch_size = batch_size
        self.n_batches = -1
        if labels_dtype == "float":
            self._lb_out_type = lambda x: x.float()
        elif labels_dtype == "long":
            self._lb_out_type = lambda x: x.long()
        elif labels_dtype == "int":
            self._lb_out_type = lambda x: x.int()


        self.training_set = None
        self.training_targets = None
        self.training_index = None
        self.validation_set = None
        self.validation_targets = None
        self.validation_index = None
        self.test_set = None
        self.test_targets = None
        self.test_index = None
        self.shuffle = shuffle

    def save_split(self, path):
        if not os.path.exists(f"{path}/dataloader"):
            os.mkdir(f"{path}/dataloader")

        if self.training_set is not None:
            np.save(f"{path}/dataloader/training_set.npy", self.training_set)
            np.save(f"{path}/dataloader/training_targets.npy", self.training_targets)
            np.save(f"{path}/dataloader/training_index.npy", self.training_index)
        if self.validation_set is not None:
            np.save(f"{path}/dataloader/validation_set.npy"       , self.validation_set)
            np.save(f"{path}/dataloader/validation_targets.npy"   , self.validation_targets)
            np.save(f"{path}/dataloader/validation_index.npy"     , self.validation_index)
        if self.test_set is not None:
            np.save(f"{path}/dataloader/test_set.npy"      , self.test_set)
            np.save(f"{path}/dataloader/test_targets.npy"  , self.test_targets)
            np.save(f"{path}/dataloader/test_index.npy"    , self.test_index)

    def load_split(self, path):
        if not os.path.exists(f"{path}/dataloader/"):
            return False

        self.training_targets = np.load(f"{path}/dataloader/training_targets.npy", allow_pickle = True)
        self.training_set = np.load(f"{path}/dataloader/training_set.npy", allow_pickle = True)
        self.training_index = np.load(f"{path}/dataloader/training_index.npy", allow_pickle = True)
        self.validation_set = np.load(f"{path}/dataloader/validation_set.npy", allow_pickle = True)
        self.validation_targets = np.load(f"{path}/dataloader/validation_targets.npy", allow_pickle = True)
        self.validation_index = np.load(f"{path}/dataloader/validation_index.npy", allow_pickle = True)
        self.test_set = np.load(f"{path}/dataloader/test_set.npy", allow_pickle = True)
        self.test_targets = np.load(f"{path}/dataloader/test_targets.npy", allow_pickle = True)
        self.test_index = np.load(f"{path}/dataloader/test_index.npy", allow_pickle = True)

    def split_sample(self, fraction_validation=0.3, fraction_test=0.0, pre_shuffle=True):
        ndata = len(self.data)
        if pre_shuffle:
            shuffler = np.random.permutation(ndata)
            data = self.data[shuffler]
            target = self.target[shuffler]
            index = self.index[shuffler]
        else:
            data = self.data
            target = self.target
            index = self.index

        if fraction_test>0.:
            # requested test sample
            lim = int(len(data)*fraction_test)
            self.test_set = data[:lim]
            self.test_targets = target[:lim]
            self.test_index = index[:lim]
            left_set = data[lim:]
            left_targets = target[lim:]
            left_index = index[lim:]
        else:
            left_set = data
            left_targets = target
            left_index = index

        lim = int(len(left_set)*fraction_validation)
        self.validation_set = left_set[:lim]
        self.validation_targets = left_targets[:lim]
        self.validation_index = left_index[:lim]
        self.training_set = left_set[lim:]
        self.training_targets = left_targets[lim:]
        self.training_index = left_index[lim:]

    def get_all_training(self):
        if self.training_set is None:
            return None, None, None

        return torch.tensor(self.training_set).float(), self._lb_out_type(torch.tensor(self.training_targets)), self.training_index

    def get_all_validation(self):
        if self.validation_set is None:
            return None, None, None

        return torch.tensor(self.validation_set).float(), self._lb_out_type(torch.tensor(self.validation_targets)), self.validation_index

    def get_all_test(self):
        if self.test_set is None:
            return None, None, None

        return torch.tensor(self.test_set).float(), self._lb_out_type(torch.tensor(self.test_targets)), self.test_index

    def iter_validation(self):
        if self.validation_set is None:
            return None, None, None

        n_batches = math.ceil(len(self.validation_set)/self.batch_size)
        for i in range(n_batches):
            data = self.validation_set[i*self.batch_size:(i+1)*self.batch_size]
            labels = self.validation_targets[i*self.batch_size:(i+1)*self.batch_size]
            index = self.validation_index[i*self.batch_size:(i+1)*self.batch_size]
            yield torch.tensor(data).float(), self._lb_out_type(torch.tensor(labels)), index

    def iter_test(self):
        if self.test_set is None:
            return None, None, None

        n_batches = math.ceil(len(self.test_set)/self.batch_size)
        for i in range(n_batches):
            data = self.test_set[i*self.batch_size:(i+1)*self.batch_size]
            labels = self.test_targets[i*self.batch_size:(i+1)*self.batch_size]
            index = self.test_index[i*self.batch_size:(i+1)*self.batch_size]
            yield torch.tensor(data).float(), self._lb_out_type(torch.tensor(labels)), index

    def __iter__(self):
        if self.training_set is None:
            return None, None, None

        self.n_batches = math.ceil(len(self.training_set)/self.batch_size)
        if self.shuffle:
            shuffler = np.random.permutation(len(self.training_set))
            self.training_set = self.training_set[shuffler]
            self.training_targets = self.training_targets[shuffler]
            self.training_index = self.training_index[shuffler]
        for i in range(self.n_batches):
            data = self.training_set[i*self.batch_size:(i+1)*self.batch_size]
            labels = self.training_targets[i*self.batch_size:(i+1)*self.batch_size]
            index = self.training_index[i*self.batch_size:(i+1)*self.batch_size]
            yield torch.tensor(data).float(), self._lb_out_type(torch.tensor(labels)), index

    def __getitem__(self, i):
        if isinstance(i, slice):
            return torch.tensor(self.data[i]).float(), torch.tensor(self.target[i]), self.index[i]
        n_batches = math.ceil(len(self.data)/self.batch_size)
        if i < n_batches:
            data = self.data[i*self.batch_size:(i+1)*self.batch_size]
            labels = self.target[i*self.batch_size:(i+1)*self.batch_size]
            index = self.index[i*self.batch_size:(i+1)*self.batch_size]
            return torch.tensor(data).float(), self._lb_out_type(torch.tensor(labels)), index
        else:
            raise IndexError


class Reporter():

    def __init__(self, model, optimizer):
        self.model = model
        self.optimizer = optimizer

    def epoch_end(self, global_epoch, global_step, metrics_dic):
        pass

    def end(self):
        pass


class TBReporter(Reporter):

    def __init__(self, model, optimizer):
        super(TBReporter, self).__init__(model, optimizer)
        self.writer = None
        self.run_name = ""
        self.set_name = ""

    def init_run(self, set_name, run_name, dataloader, force_new = False):
        new_index = 0
        if os.path.exists(f"{set_name}"):
            if force_new:
                for run_dir in [_ for _ in os.listdir(f"{set_name}") if run_name == _.split("_")[0]]:
                    shutil.rmtree(f"{set_name}/{run_dir}")
            else:
                run_dirs = [re.search(f"{run_name}\_([0-9]+)", _) for _ in os.listdir(f"{set_name}")]
                run_dirs = [to_int(_.group(1)) for _ in run_dirs if _ is not None]
                if len(run_dirs) > 0:
                    new_index = max(run_dirs) + 1

        self.set_name = set_name
        self.run_name = f"{run_name}_{new_index}"
        self.writer = SummaryWriter(f"{set_name}/{run_name}_{new_index}")
        #self.writer.add_graph(self.model, next(dataloader.iter_validation())[0])
        self.writer.add_graph(self.model, next(iter(dataloader))[0])
        self.writer.add_custom_scalars({"Metrics":
                                        {"Loss": ["Multiline", ["train/losses", "validate/losses"]]}
                                        })
        self.writer.flush()

    def resume_run(self, set_name, run_name, old_index):
        self.set_name = set_name
        self.run_name = f"{run_name}_{old_index}"
        self.writer = SummaryWriter(f"{set_name}/{run_name}_{old_index}")
        self.writer.add_custom_scalars({"Metrics":
                                        {"Loss": ["Multiline", ["train/losses", "validate/losses"]]}
                                        })
        self.writer.flush()

    def epoch_end(self, global_epoch, global_step, metrics_dic):
        if self.writer is None:
            return
        for tag, dic in metrics_dic.items():
            if isinstance(dic, dict):
                for k, v in dic.items():
                    if isinstance(v, uncertainties.core.Variable):
                        v = v.nominal_value
                    if isinstance(v, str):
                        self.writer.add_text(f"{tag}/{k}", v, global_step)
                    else:
                        self.writer.add_scalar(f"{tag}/{k}", v, global_step)
            elif isinstance(dic, str):
                self.writer.add_text(f"{tag}", dic, global_step)
            else:
                self.writer.add_scalar(tag, dic, global_step)
        self.writer.flush()

    def end(self):
        self.writer.close()

class Trainer():

    def __init__(self, model, train_dl, validate_dl, reporter, criterion, optimizer, cp_rate = 100, device = "cpu"):
        self.model = model
        self.dataloader = train_dl
        self.dataloader_val = validate_dl
        self.reporter = reporter
        self.criterion = criterion
        self.optimizer = optimizer
        self.device = device
        self.prev_report_step = 0
        self.norm = None

        self.global_epoch = 0

        self.training_losses = []
        self.checkpoint_rate = cp_rate

    def train(self, epochs, apply_norm = True):
        self.check_norm(apply_norm)
        with tqdm(total = epochs) as t:
            for _ in range(epochs):
                self.global_epoch += 1

                epoch_loss = 0.
                for data in tqdm(self.dataloader, desc = "Training", leave = False):
                    d, l = data[0].to(self.device), data[1].to(self.device)
                    # if len(l.shape)==1:
                    #     l = l.reshape([-1, 1]) # Better for some of the loss functions, but not CrossEntropyLoss?
                    # zero the parameter gradients
                    self.optimizer.zero_grad()

                    # forward + backward + optimize
                    outputs = self.model(d)
                    loss = self.criterion(outputs, l)
                    loss.backward()
                    if torch.isnan(loss):
                        raise TrainingError("Nan detected")
                    self.optimizer.step()

                    epoch_loss += loss.item()

                # print every epoch
                with torch.no_grad():
                    epoch_loss = epoch_loss / len(self.dataloader)
                    self.training_losses.append(loss.item())

                    self.compute_validation_scalars()

                    t.set_postfix(loss = epoch_loss)
                    t.update()
                if self.checkpoint_rate > 0 and (self.global_epoch % self.checkpoint_rate == 0):
                    self.save_checkpoint(f"{self.reporter.set_name}/{self.reporter.run_name}/{self.reporter.run_name}")

        self.dataloader.use_normalization(False)
        self.dataloader_val.use_normalization(False)

    def save_model(self, path = None):
        if path is None:
            path = f"{self.reporter.set_name}/{self.reporter.run_name}/model_{self.reporter.run_name}.pth"
        print(f"Saving model in {path}")
        torch.save(self.model, path)

    def save_checkpoint(self, path = None):
        if path is None:
            path = f"{self.reporter.set_name}/{self.reporter.run_name}/{self.reporter.run_name}"
        print(f"Saving checkpoint in {path}")
        torch.save({
            'model_state_dict': self.model.state_dict(),
            'optimizer_state_dict': self.optimizer.state_dict(),
            'criterion': self.criterion,
            'report_step': self.prev_report_step,
            'normalization': self.norm,
            }, f"{path}_epoch{self.global_epoch}.pth")

    def load_checkpoint(self, path, epoch):
        full_path = f"{path}_epoch{epoch}.pth"
        if not os.path.exists(full_path):
            raise FileError(f"Error: checkpoint file {full_path} does not exist")
            return

        checkpoint = torch.load(full_path)
        self.model.load_state_dict(checkpoint['model_state_dict'])
        self.optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        self.criterion = checkpoint["criterion"]
        self.global_epoch = epoch
        self.prev_report_step = checkpoint["report_step"]
        self.norm = checkpoint["normalization"]

    def show_stats(self):
        ax, fwd = self.prepare_ax()
        if ax is not None:
            ax.plot(self.training_losses, label = "Training loss")
        self.show_user_stats(fwd)

    def extract_normalization(self):
        ds_norm = self.dataloader.dataset.get_normalization()
        if ds_norm is not None:
            print("Extracting saved normalization from dataset")
            self.norm = ds_norm
            return
        print("Extracting normalization .", end = "")
        input_size = next(iter(self.dataloader))[0].shape[1]
        mean = torch.zeros((input_size))
        std = torch.zeros((input_size))
        n = 0
        for d in tqdm(self.dataloader):
            d = d[0]
            ni = d.shape[0]
            mui = torch.mean(d, dim=0)

            n += ni
            mean += ni*mui

        print(".", end = "")
        mu = mean/n
        for d in tqdm(self.dataloader):
            d = d[0]
            ni = d.shape[0]
            mui = torch.mean(d, dim=0)
            sigmai = torch.std(d, dim=0)**2

            std += (ni - 1) * sigmai + ni * (mui - mu) ** 2

        sigma = np.sqrt(std / (n - 1))
        self.norm = (mu, sigma)
        print(". Done")
        self.save_normalization_for_ds()

    def save_normalization_for_ds(self):
        if self.norm is not None:
            torch.save(self.norm, Path(self.dataloader.dataset.inputdir) / "norm.pt")

    def create_normalization_transform(self):
        if self.norm is None:
            self.extract_normalization()

        return lambda data: (data - self.norm[0]) / self.norm[1]

    def configure_normalization(self):
        self.dataloader.dataset.set_normalization_transform(self.create_normalization_transform())
        self.dataloader_val.dataset.set_normalization_transform(self.create_normalization_transform())

    def check_norm(self, apply_norm = True):
        if self.norm is not None and apply_norm:
            self.configure_normalization()
            self.dataloader.dataset.use_normalization(True)
            self.dataloader_val.dataset.use_normalization(True)

    def replace_variable(self, minibatch, var_num):
        batch_size = minibatch.shape[0]
        mu, sigma = self.norm[0][var_num], self.norm[1][var_num]
        mu = torch.full((batch_size,), mu)
        sigma = torch.full((batch_size,), sigma)
        new_values = torch.normal(mu, sigma)
        minibatch_new = minibatch.detach().clone()
        minibatch_new[:, var_num] = new_values
        return minibatch_new

    def test_var(self, var_num):
        with torch.no_grad():
            metrics, _ = self.compute_validation_scalars(False, replace_var = var_num)

        return metrics

    def check_var_impact(self, var_list):
        results = {}
        for var in var_list:
            results[var] = self.test_var(var)
        return results

    def show_user_stats(self, ax):
        pass

    def prepare_ax(self):
        return None

    def compute_validation_scalars(self, do_report = True, replace_var = None):
        pass

