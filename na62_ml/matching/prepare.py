import numpy as np

#######################################
# Functions for processing input data
# into something usable
#######################################
def mom_reduce(dataset, min_p, max_p, gtk_good_def, restrict_best_track):
    dataset.drop(dataset[(dataset["ds_Mag"]<min_p) | (dataset["ds_Mag"]>max_p)].index, inplace=True)
    data,metadata = reduce_dataset(dataset, gtk_good_def, restrict_best_track)
    #metadata[["ds_Mag", "DSMagFit"]] = data[["ds_Mag", "DSMagFit"]].values
    #data = data.drop(columns=["ds_Mag", "DSMagFit"])
    return data,metadata


def transform_dataset(dataset, gtk_good_def):
    '''
    Transform the input dataframe
      - Define good events - good gtk definition is passed as parameter
      - Merge variables
    '''
    dataset["Good"] = gtk_good_def(dataset["gtk_Good"]) & (dataset["ds_Good"] == 1)

    # Tranformations
    # - VtxCDA (x,y,z) shifted to mean beam position. z=0 or not. Maybe 0<=z<=1 (rescaled to FV position)
    beamX = dataset["BeamX"] + (dataset["Z"] - 102400.0) * dataset["BeamSlopeX"]
    beamY = dataset["BeamY"] + (dataset["Z"] - 102400.0) * dataset["BeamSlopeY"]
    dataset["VtxX"] = dataset["X"] - beamX
    dataset["VtxY"] = dataset["Y"] - beamY  # Mistake here in previous training used X column
    dataset["VtxZ"] = (dataset["Z"] - 110000.) / 180000.
    # - GTKX/Y shifted to mean beam position (or Vtx)
    dataset["GTKX_"] = dataset["GTKX"] - beamX
    dataset["GTKY_"] = dataset["GTKY"] - beamY
    # - DSX/Y shifted to mean beam position (or Vtx)
    dataset["DSX_"] = dataset["DSX"] - beamX
    dataset["DSY_"] = dataset["DSY"] - beamY
    # - Time difference
    dataset["DT_gtk"] = dataset["gtk_Time"] - dataset["ds_KTAGTime"]
    dataset["DT_ds"] = dataset["ds_Time"] - dataset["ds_KTAGTime"]  # TODO: ds_KTAGTime is used twice: here and by itself as a feature
    dataset["DT_rich"] = dataset["ds_RICHTime"] - dataset["ds_KTAGTime"]  # TODO: ds_RICHTime is used twice: here and by itself as a feature
    dataset["DT"] = dataset["ds_Time"] - dataset["gtk_Time"]

    return dataset


def gtk_good_def_mc_pure(ds):
    return ds == 1


def gtk_good_def_mc(ds):
    return (ds % 100) == 1


def gtk_good_def_k3pi(ds):
    return (ds / 100).astype(np.int32) == 1


def reduce_dataset(dataset, gtk_good_def, restrict_best_track):
    '''
    Reduce dataset take the DataFrame and applies some reduction by removing rows and columns:
      - Remove all vertices where gtk and ds are both not the primaries (they could still be a correct vertex from a different event) - good gtk definition is passed as parameter
      - Keep abs(dt)<1
      - Drop all events which are not preselected
      - If restrict_best_track, drop all events where no best track has been selected
      - Drop a series of unused columns
    '''
    # Remove all vertices where gtk and ds are both not the primaries (they could still be a correct vertex from a different event)
    primaries = (gtk_good_def(dataset["gtk_Good"]) | (dataset["ds_Good"] == 1)) & ~(dataset["ds_Time"] == -999.0) & ~(dataset["ds_RICHTime"] == -999.0)
    dataset = dataset[primaries]

    dataset = dataset[~(np.abs(dataset["DT"]) > 1)]

    dataset = dataset[~(dataset["Preselected"] == 0)]
    if restrict_best_track:
        dataset = dataset[~(dataset["BestTrackSelected"] == 0)]

    cols_to_drop = ["index", "gtk_Good", "ds_Good",
                    "BeamX", "BeamY",
                    "BeamSlopeX", "BeamSlopeY",
                    "X", "Y", "Z",
                    "GTKX", "GTKY",
                    "DSX", "DSY",
                    "ds_Time", "gtk_Time",
                    "gtk_Run", "gtk_Burst", "gtk_Event", "gtk_Index",
                    "hgtk_Run_s0", "hgtk_Burst_s0", "hgtk_Event_s0", "hgtk_GTKIndex_s0",
                    "hgtk_Run_s1", "hgtk_Burst_s1", "hgtk_Event_s1", "hgtk_GTKIndex_s1",
                    "hgtk_Run_s2", "hgtk_Burst_s2", "hgtk_Event_s2", "hgtk_GTKIndex_s2",
                    "ds_Run", "ds_Burst", "ds_Event", "ds_Index",
                    "Preselected", "BestTrackSelected",
                    'gtk_GTKAssocQ1', 'gtk_GTKAssocQ2', 'gtk_GTKAssocVtx_X',
                    'gtk_GTKAssocVtx_Y', 'gtk_GTKAssocVtx_Z', 'gtk_GTKAssocP_X',
                    'gtk_GTKAssocP_Y', 'gtk_GTKAssocP_Z', 'gtk_GTKAssocDSP_X',
                    'gtk_GTKAssocDSP_Y', 'gtk_GTKAssocDSP_Z',
                    "ds_BestTrack",
                    "hgtk_HitStation_s0", "hgtk_HitStation_s1", "hgtk_HitStation_s2",
                    "lambdaGTK"
                    ]
    metadata  = dataset[cols_to_drop]
    data = dataset.drop(columns = cols_to_drop)
    return data, metadata