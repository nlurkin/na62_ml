'''
Created on 26 Jan 2021

@author: nlurkin
'''

import math

import numpy as np
import pandas as pd
import na62_ml.data as data
from na62_ml.ml_lib import DataLoader
from tqdm.autonotebook import tqdm

from . import io, prepare


#######################################
# Functions for loading data directly
# in memory, with checkpoints
#######################################
class Deprecated:
    def load_by_chunks(file_path, chunk_size, max_chunk = None):
        b_chunk = 0
        e_chunk = chunk_size
        if max_chunk is None:
            max_chunk = data.io.get_file_last_entryID(file_path, "Event")
        if max_chunk == -1:
            print("Cannot find last event in file")
            return None

        dataset = None
        with tqdm(total = math.ceil(max_chunk / chunk_size)) as t:
            while b_chunk < max_chunk:
                dfs = io.load_export(file_path, b_chunk, e_chunk)
                if dataset is None:
                    dataset = io.assemble_dataset(*dfs)
                else:
                    new_dataset = io.assemble_dataset(*dfs)
                    dataset = pd.concat([dataset, new_dataset])
                b_chunk = e_chunk
                e_chunk += chunk_size
                t.update()

        return dataset


    def prepare_data(data_dir):
        dataset = data.io.load_or_generate("dataset.pqet", data_dir, lambda: Deprecated.load_by_chunks(f"{data_dir}/export.csv", 10000))
        if dataset is None:
            return None, None, None

        dataset = data.io.load_or_generate("dataset_trans.pqet", data_dir, lambda: prepare.transform_dataset(dataset))

        dataset, _ = prepare.reduce_dataset(dataset)

        # select randomly same amount of bad data as good data
        n_good = sum(dataset["Good"] == True)
        n_bad = sum(dataset["Good"] == False)
        bad_only = dataset.loc[dataset["Good"] == False]
        try:
            drop_indices = np.random.choice(bad_only.index, n_bad - n_good, replace = False)
            sub_dataset = dataset.drop(drop_indices)
        except ValueError as e:
            print(n_good, n_bad)
            print(e)

        labels = sub_dataset["Good"]
        data = dataset.drop(columns = ["Good"])

        print("Preparing data loader")
        dl = DataLoader(data, labels, labels_dtype = "long")

        if not dl.load_split(data_dir):
            dl.split_sample(fraction_test = 0.1)
            dl.save_split(data_dir)

        return dl, data, dataset


