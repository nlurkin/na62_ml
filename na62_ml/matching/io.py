from pathlib import Path

import numpy as np
import pandas as pd
import uproot
from na62_ml.data.io import find_files_with_regex


#######################################
# Functions dealing with early csv
# files containting mixed DataFrames
#######################################
def load_export(file_path, i_start=0, i_end=None):
  event = []
  gtk_tracks = []
  ds_tracks = []
  vtx = []

  evt_header = []
  gtk_header = ["Event"]
  ds_header = ["Event"]
  vtx_header = ["Event"]

  curr_evt = None
  with open(file_path, 'r') as fd:
    for line in fd:
      line = line.strip()
      fields = line.split(";")
      if curr_evt is not None and (int(curr_evt) < i_start) and fields[0] != "Event":
        continue

      if fields[0] == "Event":
        if len(evt_header) == 0:
          evt_header = fields[1:]
        else:
          curr_evt = fields[1]
          if int(curr_evt) >= i_end:
            break
          if int(curr_evt) >= i_start:
            event.append(fields[1:])
      else:
        vals = [curr_evt]
        vals.extend(fields[1:])
      if fields[0] == "GTK":
        if len(gtk_header) == 1:
          gtk_header.extend(fields[1:])
        else:
          gtk_tracks.append(vals)
      if fields[0] == "DS":
        if len(ds_header) == 1:
          ds_header.extend(fields[1:])
        else:
          ds_tracks.append(vals)
      if fields[0] == "Vtx":
        if len(vtx_header) == 1:
          vtx_header.extend(fields[1:])
        else:
          vtx.append(vals)

  df_evt = pd.DataFrame(
      event, columns=evt_header, dtype=np.float
  ).astype({'index': np.int32}).set_index("index")
  df_gtk = pd.DataFrame(
      gtk_tracks, columns=gtk_header, dtype=np.float
  ).astype({'Event': np.int32, 'index': np.int32, "Good": np.bool}).set_index(["Event", "index"]).add_prefix('gtk_')
  df_ds = pd.DataFrame(
      ds_tracks, columns=ds_header, dtype=np.float
  ).astype({'Event': np.int32, 'index': np.int32, "Good": np.bool}).set_index(["Event", "index"]).add_prefix('ds_')
  df_vtx = pd.DataFrame(
      vtx, columns=vtx_header, dtype=np.float
  ).astype({'Event': np.int32, 'GTKIndex': np.int32, "DSIndex": np.int32}).set_index(["Event", "GTKIndex", "DSIndex"], drop=False)

  return df_evt, df_gtk, df_ds, df_vtx


def assemble_dataset(df_evt, df_gtk, df_ds, df_vtx):
  entries = []
  indices = []
  for evt in df_evt.index:
    for index, entry in df_vtx.loc[evt].iterrows():
      entry = entry.append(df_evt.loc[evt])
      entry = entry.append(df_gtk.loc[evt, index[0]])
      entry = entry.append(df_ds.loc[evt, index[1]])
      entries.append(entry)
      indices.append(index)

  dataset = pd.DataFrame(entries).set_index(["Event", "GTKIndex", "DSIndex"])
  return dataset


def load_single_file(file_path, i_start, i_end):
  dfs = load_export(file_path, i_start, i_end)
  return assemble_dataset(*dfs)


#######################################
# Functions for dealing with multi-csv
# datasets
#######################################
def load_export_from_subfile(path, template, subs, i_start=0, i_end=None):
  files = [f"{path}/{template.replace('*', s['name'])}" for s in subs]
  subfiles = {s['name']: None for s in subs}

  i_index_start = None
  i_index_end = None
  for file, sub in zip(files, subs):
    subfiles[sub['name']] = pd.read_csv(file, delimiter=";", dtype=np.float)

    if 'dtype' in sub:
      subfiles[sub['name']] = subfiles[sub['name']].astype(sub['dtype'])
    if "prefix" in sub:
      subfiles[sub['name']] = subfiles[sub['name']].add_prefix(sub["prefix"])

    if len(subfiles[sub["name"]])==0:
      continue
    if "index" in sub:
      subfiles[sub['name']] = subfiles[sub['name']].set_index(sub['index'])
    subfiles[sub["name"]].sort_index(inplace=True)

    if i_index_start is None:
      if i_end is None:
        subfiles[sub["name"]] = subfiles[sub["name"]].iloc[i_start:]
      else:
        subfiles[sub["name"]] = subfiles[sub["name"]].iloc[i_start:i_end]
      i_index_start = subfiles[sub["name"]].iloc[0].name
      i_index_end = subfiles[sub["name"]].iloc[-1].name
    else:
      subfiles[sub['name']] = subfiles[sub['name']].loc[i_index_start:i_index_end]
    subfiles[sub["name"]] = subfiles[sub["name"]].reset_index(drop=False)

  return subfiles


def assemble_dataset_2(datasets):
  m = pd.merge(left=datasets["evt"], right=datasets["vtx"],
               left_on=["Run", "Burst", "Event"], right_on=["Run", "Burst", "Event"])
  m = pd.merge(left=m, right=datasets["gtk"], left_on=[
               "Run", "Burst", "Event", "GTKIndex"], right_on=["gtk_Run", "gtk_Burst", "gtk_Event", "gtk_Index"])
  m = pd.merge(left=m, right=datasets["ds"], left_on=[
               "Run", "Burst", "Event", "DSIndex"], right_on=["ds_Run", "ds_Burst", "ds_Event", "ds_Index"])

  hgtk = datasets["hgtk"].groupby(
      ["hgtk_Run", "hgtk_Burst", "hgtk_Event", "hgtk_GTKIndex", "hgtk_HitStation"]).mean().reset_index()
  s0 = hgtk.loc[hgtk["hgtk_HitStation"] == 1].add_suffix("_s0")
  s1 = hgtk.loc[hgtk["hgtk_HitStation"] == 2].add_suffix("_s1")
  s2 = hgtk.loc[hgtk["hgtk_HitStation"] == 3].add_suffix("_s2")

  m = pd.merge(left=m, right=s0, left_on=["Run", "Burst", "Event", "GTKIndex"], right_on=[
               "hgtk_Run_s0", "hgtk_Burst_s0", "hgtk_Event_s0", "hgtk_GTKIndex_s0"], suffixes=(None, "_s0"))
  m = pd.merge(left=m, right=s1, left_on=["Run", "Burst", "Event", "GTKIndex"], right_on=[
               "hgtk_Run_s1", "hgtk_Burst_s1", "hgtk_Event_s1", "hgtk_GTKIndex_s1"], suffixes=(None, "_s1"))
  m = pd.merge(left=m, right=s2, left_on=["Run", "Burst", "Event", "GTKIndex"], right_on=[
               "hgtk_Run_s2", "hgtk_Burst_s2", "hgtk_Event_s2", "hgtk_GTKIndex_s2"], suffixes=(None, "_s2"))

  m = m.reset_index()
  m = m.set_index(["Run", "Burst", "Event", "GTKIndex", "DSIndex"])
  return m


def load_export_sf(path, i_start, i_end, file_sizes):
  '''
  ### Load function
  Loads a dataframe from an input path. Extracts only the specified event range.
  File_sizes is an array containing the numbre of events in each file
  Important: the path should point to actual files, however it can be a glob expression
  (so the string here should be one that gives a good list of files with `ls`)
  '''
  path = Path(path)
  file_sizes = np.cumsum(file_sizes)
  files = find_files_with_regex(path.parent, path.name)
  file_start = file_sizes.searchsorted(i_start, side="right")

  dfs = []

  while i_start < i_end and file_start < len(file_sizes):
    temp_end = min(file_sizes[file_start] + 1, i_end)
    offset = file_sizes[file_start - 1] if file_start > 0 else 0
    lpath = Path(files[file_start])
    subfiles = load_export_from_subfile(lpath.parent, lpath.name, [
        {"name": "evt", "dtype": {'Run': np.int32, 'Burst': np.int32, 'Event': np.int32,
                                  "Preselected": bool, "BestTrackSelected": bool}, "index": ["Run", "Burst", "Event"]},
        {"name": "ds", "prefix": "ds_", "dtype": {'Run': np.int32, 'Burst': np.int32, 'Event': np.int32, 'Index': np.int16,
                                                  "Good": np.int32, "KTAGSectors": np.int8, "BestTrack": bool},
                                                  "index": ["ds_Run", "ds_Burst", "ds_Event"]},
        {"name": "gtk", "prefix": "gtk_", "dtype": {
            'Run': np.int32, 'Burst': np.int32,
            'Event': np.int32, 'Index': np.int16, "Good": np.int32},
            "index": ["gtk_Run", "gtk_Burst", "gtk_Event"]},
        {"name": "hgtk", "prefix": "hgtk_", "dtype": {
            'Run': np.int32, 'Burst': np.int32,
            'Event': np.int32, 'GTKIndex': np.int16, "HitStation": np.int8},
            "index": ["hgtk_Run", "hgtk_Burst", "hgtk_Event"]},
        {"name": "vtx", "dtype": {'Run': np.int32, 'Burst': np.int32, 'Event': np.int32,
                                  'GTKIndex': np.int16, "DSIndex": np.int16},
                                  "index": ["Run", "Burst", "Event"]}
    ], i_start - offset, temp_end - offset)
    df = assemble_dataset_2(subfiles)
    if file_start > 0:
      df = df.reset_index()
      df["index"] += offset
      df = df.set_index(["Run", "Burst", "Event", "GTKIndex", "DSIndex"])

    dfs.append(df)

    i_start = temp_end
    file_start += 1
  return pd.concat(dfs)


def load_export_hits(path, i_start, i_end, file_sizes):
  '''
  ### Load function
  Loads a dataframe from an input path. Extracts only the specified event range.
  File_sizes is an array containing the numbre of events in each file
  Important: the path should point to actual files, however it can be a glob expression
  (so the string here should be one that gives a good list of files with `ls`)
  '''
  path = Path(path)
  file_sizes = np.cumsum(file_sizes)
  files = find_files_with_regex(path.parent, path.name)

  file_start = file_sizes.searchsorted(i_start, side="right")

  dfs = []

  while i_start < i_end and file_start < len(file_sizes):
    temp_end = min(file_sizes[file_start] + 1, i_end)
    offset = file_sizes[file_start - 1] if file_start > 0 else 0
    lpath = Path(files[file_start])
    subfiles = load_export_from_subfile(lpath.parent, lpath.name, [
        {"name": "main", "dtype": {'Run': np.int32, 'Burst': np.int32, 'Event': np.int32}, "index": ["Run", "Burst", "Event"]}],
                                        i_start - offset, temp_end - offset)
    df = subfiles["main"]
    if file_start > 0:
      df = df.reset_index()
      df["index"] += offset
      df = df.set_index(["Run", "Burst", "Event"])

    dfs.append(df)

    i_start = temp_end
    file_start += 1
  return pd.concat(dfs)


def load_root_files(path, i_start, i_end, file_sizes):
    path = Path(path)
    file_sizes = np.cumsum(file_sizes)
    if "*" in str(path):
        files = find_files_with_regex(path.parent, path.name)
    else:
        files = [path]
    file_start = file_sizes.searchsorted(i_start, side="right")

    dfs = []

    while i_start < i_end and file_start < len(file_sizes):
        temp_end = min(file_sizes[file_start] + 1, i_end)
        offset = file_sizes[file_start - 1] if file_start > 0 else 0
        lpath = Path(files[file_start])

        fd = uproot.open(lpath)
        df = fd["KpiMatchInfo"].arrays(library="pd", )
        df = df.iloc[i_start-offset:temp_end-offset]
        df["Correctmatch"] = df["Correctmatch"].astype("int")

        if file_start > 0:
            df = df.reset_index()
            df["index"] += offset
            df = df.set_index(["index", "GTKTrackIndex"])

        dfs.append(df)

        i_start = temp_end
        file_start += 1
    return pd.concat(dfs)
