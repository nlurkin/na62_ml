import numpy as np
import pandas as pd

#######################################
# Functions for processing input data
# into something usable
#######################################

def transform_dataset(dataset):
    dataset["Good"] = dataset["GTKTrackIndex"]==dataset["correctGTKTrackIndex"]

    dataset = dataset.rename(columns={"event": "Event", "run": "Run", "burst": "Burst"})

    # Tranformations
    # - VtxCDA (x,y,z) shifted to mean beam position. z=0 or not. Maybe 0<=z<=1 (rescaled to FV position)
    # - GTKX/Y shifted to mean beam position (or Vtx)
    # - DSX/Y shifted to mean beam position (or Vtx)
    # - Time difference
    dataset["DT_gtk01"] = dataset["tGTK0"] - dataset["tGTK1"]
    dataset["DT_gtk02"] = dataset["tGTK0"] - dataset["tGTK2"]
    dataset["DT_gtk03"] = dataset["tGTK0"] - dataset["tGTK3"]
    dataset = pd.get_dummies(dataset, columns=["CandType"])

    dataset["DX_gtk01"] = dataset["xGTK0"] - dataset["xGTK1"]
    dataset["DX_gtk02"] = dataset["xGTK0"] - dataset["xGTK2"]
    dataset["DX_gtk03"] = dataset["xGTK0"] - dataset["xGTK3"]
    dataset["DY_gtk01"] = dataset["yGTK0"] - dataset["yGTK1"]
    dataset["DY_gtk02"] = dataset["yGTK0"] - dataset["yGTK2"]
    dataset["DY_gtk03"] = dataset["yGTK0"] - dataset["yGTK3"]

    dataset["DTx"] = dataset["txGTK"] - dataset["txSTRAW"]
    dataset["DTy"] = dataset["tyGTK"] - dataset["tySTRAW"]
#     dataset["DX_tgtk"] = dataset["xGTK0"] - dataset["txGTK"]
#     dataset["DY_tgtk"] = dataset["yGTK0"] - dataset["tyGTK"]

#     dataset["DX_tSTRAW"] = dataset["xSTRAW"] - dataset["txSTRAW"]
#     dataset["DY_tSTRAW"] = dataset["ySTRAW"] - dataset["tySTRAW"]

    dataset["PDiff"] = dataset["pGTK"] - dataset["pSTRAW"]

    return dataset

def reduce_dataset(dataset):
    dataset.drop(dataset[np.abs(dataset["dt_GTK_RICH"]) > 2].index, inplace = True)

    cols_to_drop = ["Run", "Burst", "Event", "zSTRAW", "zGTK3",
                    "Correctmatch", "GTKTrackIndex", "correctGTKTrackIndex",
                    #'dtGTK0_min', 'dtGTK1_min', 'dtGTK2_min', 'dtGTK3_min',
                    #'dp_min', 'dtx_min', 'dty_min',
                    "cda",
                    "tGTK0", "tGTK1", "tGTK2", "tGTK3",
                    "xGTK0", "xGTK1", "xGTK2", "xGTK3",
                    "yGTK0", "yGTK1", "yGTK2", "yGTK3",
                    #"txGTK", "tyGTK",
                    #"xSTRAW", "ySTRAW",
                    "txSTRAW", "tySTRAW",
                    "pSTRAW"
                    ]
    metadata  = dataset[cols_to_drop]
    data = dataset.drop(columns = cols_to_drop)
    return data, metadata
