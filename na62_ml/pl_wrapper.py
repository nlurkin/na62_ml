'''
Created on 19 Feb 2021

@author: Nicolas Lurkin
'''

import importlib
import os
import sys

from na62_ml.ml_lib import ConfigError
from na62_ml.overloads import (GTKDSDataModule, SummaryCurves,
                               TrackMatchingScalars)
from na62_ml.pl_lib import configure_logger, configure_trainer, find_checkpoint

def_module = None

def create_network_from_file(file):
    global def_module
    if def_module is not None and def_module.__file__ != os.path.abspath(file):
        def_module = None
    basepath = os.path.dirname(os.path.abspath(file))
    file = os.path.splitext(os.path.basename(file))[0]
    sys.path.insert(0, basepath)
    if def_module is not None:
        importlib.reload(def_module)
    else:
        def_module = importlib.import_module(file)
    if not hasattr(def_module, "pl_get_model"):
        raise ConfigError(f"Provided configuration module does not provide pl_get_model()")

    if hasattr(def_module, "config_metrics"):
        def_module.config_metrics()

    model = def_module.pl_get_model()
    return model


def load_disktsdata(data_dir, data_limit = None, batch_size = 10, shuffle = True, drop_last = True, num_workers = 0, batch_sampler = None):
    return GTKDSDataModule(data_dir, data_limit=data_limit, batch_size=batch_size, shuffle=shuffle,
                           drop_last=drop_last, num_workers=num_workers, batch_sampler=batch_sampler)
    # data.create_normalization_transform()
    # return dl_train, dl_validate, dl_test

def create_from_scratch(fn_create_model, trainerClass=None, data=None, max_epochs=1, log_dir = "runs", callbacks=None):
    model = fn_create_model()

    print(f"Creating model at: {log_dir}")
    print(f"  Name: {model.get_name()}")
    logger = configure_logger(log_dir, model.get_name())

    if callbacks is None:
        callbacks = [TrackMatchingScalars(), SummaryCurves()]
    trainer = configure_trainer(logger, max_epochs, callbacks=callbacks)

    return trainer, model


def load_from_checkpoint(fn_get_modelclass, log_dir, version="last", trainerClass=None, data=None, epoch="last", max_epochs=None, new_epochs=1, callbacks = None):
    modelclass = fn_get_modelclass()
    model_name = modelclass.get_name()
    checkpt, version, epoch = find_checkpoint(log_dir, model_name, version, epoch)

    model = modelclass.load_from_checkpoint(checkpt)
    model.eval()

    # data.set_use_norm(True)
    if new_epochs is not None:
        max_epochs = epoch+new_epochs if max_epochs is None else min(max_epochs, epoch+new_epochs)

    print(f"Restoring from checkpoint {checkpt}")
    logger = configure_logger(log_dir, model_name, resume_version=version)

    if callbacks is None:
        callbacks = [TrackMatchingScalars(), SummaryCurves()]

    trainer = configure_trainer(logger, max_epochs, from_checkpoint=checkpt, callbacks=callbacks)
    return trainer, model

def train(trainer, model, data):
    trainer.fit(model, datamodule=data)
