'''
Created on 1 Apr 2021

@author: Nicolas Lurkin
'''

import numpy as np
import matplotlib.pyplot as plt


def gtk_algo_efficiency_parallel_pre(ds):
    nevts = len(ds.groupby("Event").first())

    # # Process only events that have a good primary particle
    primary = ds.loc[ds.index.get_level_values(0).isin(ds.loc[ds["ds_Good"] & ds["gtk_Good"]].groupby("Event").first().index)]
    nprimary = len(primary.groupby("Event").first())

    # # Number of preselected events
    pre = primary.loc[primary["Preselected"]]
    npre = len(pre.reset_index().groupby("Event").count())
    # # Number of events where a best track is selected
    best = pre.loc[pre["BestTrackSelected"]]
    nsel = len(best.groupby("Event").count())
    # # Number of events where the selected best track is the primary
    correct_ds_assoc = best.loc[best["ds_Good"] & best["ds_BestTrack"]]
    ngoodtrack = len(correct_ds_assoc.groupby("Event").count())
    # # Number of events where the primary track is not the best
    nbadtrack = len(best.loc[best["ds_Good"] & ~best["ds_BestTrack"]].groupby("Event").count())

    sorted_gtk = correct_ds_assoc.loc[correct_ds_assoc["gtk_GTKAssocVtx_Z"] != 0].sort_values(["Event", "gtk_GTKAssocQ1"], ascending = [True, False])

    # gtk_assoc = .reset_index().drop_duplicates(["Event"], keep="last").set_index(["Event", "GTKIndex", "DSIndex"])
    return {"nevts":nevts, "nprimary":nprimary, "npre":npre, "nsel":nsel, "ngoodtrack":ngoodtrack}, sorted_gtk


def gtk_algo_efficiency_parallel_post(sorted_gtk, selection_function):
    gtk_assoc = selection_function(sorted_gtk)
    assoc_count = gtk_assoc.groupby("Event").count()
    ngtkassoc = len(assoc_count)

    gtkassoc_single = gtk_assoc.loc[gtk_assoc.index.get_level_values(0).isin(assoc_count.loc[assoc_count["gtk_Good"] == 1].index)]
    ngtkassoc_single = len(gtkassoc_single)
    gtkok = gtkassoc_single.loc[gtkassoc_single["gtk_Good"]]
    ngtkok = len(gtkok)
    gtknok = gtkassoc_single.loc[~gtkassoc_single["gtk_Good"]]
    ngtknok = len(gtknok)

    return {"ngtkassoc": ngtkassoc, "ngtkassoc_single": ngtkassoc_single, "ngtkok": ngtkok}, gtkok


def gtk_algo_efficiency_procedural(ds, selection_function):
    nevts = 0
    nprimary = 0
    npre = 0
    nsel = 0
    ngoodtrack = 0
    ngtkassoc = 0
    ngtkok = 0

    for eventID in ds.groupby("Event").first().index.get_level_values(0).drop_duplicates():
        nevts += 1
        event = ds.loc[eventID]

        good_vtx = event.loc[event["gtk_Good"] & event["ds_Good"]]
        if len(good_vtx) == 0:
            continue
        nprimary += 1
        if len(good_vtx) > 1:
            print("Issue with {EventID}: more than one good vertex")
            break

        good_vtx = good_vtx.squeeze()
        if not good_vtx["Preselected"]:
            continue
        npre += 1

        if not good_vtx["BestTrackSelected"]:
            continue
        nsel += 1

        if not good_vtx["ds_BestTrack"]:
            continue
        ngoodtrack += 1

        gtk_algoed = event.loc[event["gtk_GTKAssocVtx_Z"] != 0].groupby("GTKIndex").first()
        if len(gtk_algoed) == 0:
            continue
        ngtkassoc += 1

        # sel_gtk = gtk_algoed.sort_values("gtk_GTKAssocQ1", ascending=False).iloc[0]
        sel_gtk = selection_function(gtk_algoed.sort_values("gtk_GTKAssocQ1", ascending = False))

        if not sel_gtk["gtk_Good"]:
            continue
        ngtkok += 1

        if sel_gtk["gtk_Index"] != good_vtx["gtk_Index"]:
            print(f"Problem with {eventID}: gtk_Index mismatch")

    return {"nevts":nevts, "nprimary":nprimary, "npre":npre, "nsel":nsel, "ngoodtrack":ngoodtrack, "ngtkassoc": ngtkassoc, "ngtkok": ngtkok}, None


def print_algo_efficiency(nevts, nprimary, npre, nsel, ngoodtrack, ngtkassoc, ngtkassoc_single, ngtkok, *args):
    print(f"Nevents {nevts}")
    print(f"Nevents with primary {nprimary}")
    print(f"Nevents preselected with primary {npre}")
    print(f"Nevents preselected with primary and BestTrack (BT) {nsel}")
    print(f"Nevents preselected with primary BT {ngoodtrack}")
    print(f"Nevents preselected with primary BT and GTK association {ngtkassoc}")
    print(f"Nevents preselected with primary BT and correct GTK association {ngtkok}")

    ngtknok = ngtkassoc_single - ngtkok
    print()
    print(f"Preselection efficiency: {nsel}/{nevts} ({nsel/nevts:.2%})")
    print(f"Preselection accuracy  : {ngoodtrack}/{nsel} ({ngoodtrack/nsel:.2%})")
    print(f"Association rate       : {ngtkassoc}/{ngoodtrack} ({ngtkassoc/ngoodtrack:.2%})")
    print(f"Single Association rate: {ngtkassoc_single}/{ngoodtrack} ({ngtkassoc_single/ngoodtrack:.2%})")
    print(f"Mismatch rate          : {ngtknok}/{ngtkassoc_single} ({ngtknok/ngtkassoc_single:.2%})")


def select_highest(df):
    if "Event" in df.index.names:
        return df.groupby("Event").first()
    else:
        return df.iloc[0]


def select_threshold(df, threshold):
    return df.loc[df["gtk_GTKAssocQ1"] > threshold]


def compute_auc_curve(ds, test_points):
    tpr = []
    fpr = []
    ar = []
    sar = []
    evtn, associated_gtk = gtk_algo_efficiency_parallel_pre(ds)
    for thr in test_points:
        d, _ = gtk_algo_efficiency_parallel_post(associated_gtk, lambda x: select_threshold(x, thr))
        nevts = evtn["ngoodtrack"]
        nassoc = d["ngtkassoc"]
        nassoc_single = d["ngtkassoc_single"]
        true_positive = d["ngtkok"]
        false_positive = nassoc_single - true_positive

        assoc_rate = nassoc / nevts
        single_rate = nassoc_single / nevts
        if nassoc_single > 0:
            true_rate = true_positive / nassoc_single
            false_rate = false_positive / nassoc_single
        else:
            true_rate = np.nan
            false_rate = np.nan

        ar.append(assoc_rate)
        sar.append(single_rate)
        tpr.append(true_rate)
        fpr.append(false_rate)

    return tpr, fpr, ar, sar, associated_gtk


def display_auc_curve(ds):
    test_points = np.arange(0, 1, 0.01)
    tpr, fpr, ar, sar, associated_gtk = compute_auc_curve(ds, test_points)

    stpr, sfpr = zip(*sorted(zip(tpr, fpr), key = lambda x: x[0]))
    plt.figure()
    plt.plot(sfpr, stpr)
    plt.figure()
    plt.plot(test_points, fpr, label = "False positive")
    plt.plot(test_points, tpr, label = "True positive")
    plt.plot(test_points, ar, label = "Association")
    plt.plot(test_points, sar, label = "Single assoc")
    plt.legend()

    associated_gtk.loc[associated_gtk["gtk_Good"]]["gtk_GTKAssocQ1"].hist(bins = 100, density = True, label = "Correct matching")
    associated_gtk.loc[~associated_gtk["gtk_Good"]]["gtk_GTKAssocQ1"].hist(bins = 100, density = True, label = "Mis-match")
    plt.ylim(0, 5)
    plt.legend()
