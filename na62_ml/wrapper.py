'''
Created on 19 Feb 2021

@author: Nicolas Lurkin
'''

import importlib
import os
import re
import sys

import torch

from na62_ml.ml_lib import (ConfigError, DiskTensorDataset, FileError,
                            TBReporter)

def_module = None

def create_network_from_file(file):
    global def_module
    if def_module is not None and def_module.__file__ != os.path.abspath(file):
        def_module = None
    basepath = os.path.dirname(os.path.abspath(file))
    file = os.path.splitext(os.path.basename(file))[0]
    sys.path.insert(0, basepath)
    if def_module is not None:
        importlib.reload(def_module)
    else:
        def_module = importlib.import_module(file)
    if not hasattr(def_module, "get_model"):
        raise ConfigError(f"Provided configuration module does not provide get_model()")
    if not hasattr(def_module, "get_loss_fn"):
        raise ConfigError(f"Provided configuration module does not provide get_loss_fn()")
    if not hasattr(def_module, "get_optimizer"):
        raise ConfigError(f"Provided configuration module does not provide get_optimizer()")
    if not hasattr(def_module, "get_name"):
        raise ConfigError(f"Provided configuration module does not provide get_name()")

    model = def_module.get_model()
    loss_function = def_module.get_loss_fn()
    optimizer = def_module.get_optimizer()
    model_name = def_module.get_name()

    return (model, loss_function, optimizer, model_name)


def load_disktsdata(data_dir, data_limit = None, batch_size = 10, shuffle = True, drop_last = True, num_workers = 0, batch_sampler = None):
    # Check that all input directories exist
    if not os.path.exists(data_dir):
        raise FileError(f"Directory does not exist {data_dir}")
    if not os.path.exists(f"{data_dir}/train"):
        raise FileError(f"train directory not found in {data_dir}")
    if not os.path.exists(f"{data_dir}/validate"):
        raise FileError(f"train directory not found in {data_dir}")

    # Create datasets
    train_set = DiskTensorDataset(f"{data_dir}/train", size_limit = data_limit)
    validate_set = DiskTensorDataset(f"{data_dir}/validate",
                                     size_limit = None if data_limit is None else int(data_limit / 10))
    test_set = None

    # Create DataLoaders
    dl_options = {}
    if batch_sampler is not None:
        print("Using batch_sampler for dataloader ... disabling batch_size, shuffle and drop_last options")
        dl_options["batch_sampler"] = None
    else:
        dl_options["batch_size"] = batch_size
        dl_options["shuffle"] = shuffle
        dl_options["drop_last"] = drop_last
        dl_options["num_workers"] = num_workers

    if "batch_sampler" in dl_options:
        dl_options["batch_sampler"] = batch_sampler(train_set)
    dl_train = torch.utils.data.DataLoader(train_set, **dl_options)
    if "batch_sampler" in dl_options:
        dl_options["batch_sampler"] = batch_sampler(validate_set)
    dl_validate = torch.utils.data.DataLoader(validate_set, **dl_options)

    dl_test = None

    # If test dir is found, load it as well
    if os.path.exists(f"{data_dir}/test"):
        test_set = DiskTensorDataset(f"{data_dir}/test",
                                     size_limit = None if data_limit is None else int(data_limit / 10))
        if "batch_sampler" in dl_options:
            dl_options["batch_sampler"] = batch_sampler(test_set)
        dl_test = torch.utils.data.DataLoader(test_set, **dl_options)

    return dl_train, dl_validate, dl_test


def create_from_scratch(fn_create_model, trainerClass, data, max_epochs=1, log_dir = "runs"):
    dl_train, dl_validate, _ = data
    model, loss_fn, optimizer, model_name = fn_create_model()
    reporter = TBReporter(model, optimizer)
    print(f"Creating model at: {log_dir}")
    print(f"  Name: {model_name}")
    reporter.init_run(log_dir, model_name, dl_train)
    trainer = trainerClass(model, dl_train, dl_validate, reporter, loss_fn, optimizer, cp_rate = 1)

    trainer.save_model()
    trainer.save_checkpoint()
    trainer.nepochs = max_epochs

    return trainer, model

def load_from_checkpoint(fn_create_model, log_dir, version, trainerClass, data, epoch = "last", max_epochs=None, new_epochs=1):
    dl_train, dl_validate, _ = data
    model, loss_fn, optimizer, model_name = fn_create_model()

    full_path = f"{log_dir}/{model_name}_{version}/"

    if not os.path.exists(full_path):
        raise ConfigError(f"Checkpoint path does not exist {full_path}")

    if epoch == "last":
        avail_epochs = [int(re.search(f"{model_name}_{version}_epoch([0-9]+).pth", _).group(1)) for _ in os.listdir(full_path) if "epoch" in _]
        epoch = sorted(avail_epochs)[-1]
    else:
        epoch = int(epoch)

    reporter = TBReporter(model, optimizer)
    reporter.resume_run(log_dir, model_name, version)
    trainer = trainerClass(model, dl_train, dl_validate, reporter, loss_fn, optimizer, cp_rate = 1)
    print(f"Loading checkpoint at {full_path}")
    print(f"  Name: {model_name}")
    print(f"  Run index: {version}")
    print(f"  Restarting from epoch: {epoch}")
    trainer.load_checkpoint(f"{full_path}/{model_name}_{version}", epoch)
    trainer.nepochs = new_epochs

    return trainer, model

def train(trainer, model=None, data=None):
    trainer.train(trainer.nepochs)
    trainer.save_checkpoint()
