import hashlib
import math
import os
import random

import numpy as np
import pandas as pd
import torch
from na62_ml.ml_lib import ConfigError
from tqdm.autonotebook import tqdm

from . import io


#######################################
# Functions for creating disk datasets
#######################################
def extract_train_validate_test(outdir, list_hashes, frac_validate, frac_test):
    '''
    Give the output dir, the list of hashes and the fractions of validate and test samples requested,
    split the different chunks into the train/validate/test directories. It also removes the samples from
    the initial root directory.
    '''
    avail_hashes = set(list_hashes)
    nsamples = len(avail_hashes)

    n_validate = int(nsamples * frac_validate)
    if frac_validate > 0:
        n_validate = max(1, n_validate)
    nsamples -= n_validate
    n_test = int(nsamples * frac_test)
    if frac_test > 0:
        n_test = max(1, n_test)
    nsamples -= n_validate

    if nsamples < 0:
        raise ConfigError(f"Not enough file samples ({len(avail_hashes)}) for the split val={frac_validate}, test={frac_test}")

    io.mkpath(f"{outdir}/train/labels")
    io.mkpath(f"{outdir}/train/sets")
    io.mkpath(f"{outdir}/train/meta")

    val_samples = set()
    if n_validate > 0:
        val_samples = set(random.sample(avail_hashes, n_validate))
        avail_hashes = avail_hashes - val_samples
        io.mkpath(f"{outdir}/validate/labels")
        io.mkpath(f"{outdir}/validate/sets")
        io.mkpath(f"{outdir}/validate/meta")

    test_samples = set()
    if n_test > 0:
        test_samples = set(random.sample(avail_hashes, n_test))
        avail_hashes = avail_hashes - test_samples
        io.mkpath(f"{outdir}/test/labels")
        io.mkpath(f"{outdir}/test/sets")
        io.mkpath(f"{outdir}/test/meta")

    for h in val_samples:
        os.rename(f"{outdir}/labels/labels_{h}.pt", f"{outdir}/validate/labels/labels_{h}.pt")
        os.rename(f"{outdir}/sets/data_{h}.pt", f"{outdir}/validate/sets/data_{h}.pt")
        os.rename(f"{outdir}/meta/metadata_{h}.pqet", f"{outdir}/validate/meta/metadata_{h}.pqet")

    for h in test_samples:
        os.rename(f"{outdir}/labels/labels_{h}.pt", f"{outdir}/test/labels/labels_{h}.pt")
        os.rename(f"{outdir}/sets/data_{h}.pt", f"{outdir}/test/sets/data_{h}.pt")
        os.rename(f"{outdir}/meta/metadata_{h}.pqet", f"{outdir}/test/meta/metadata_{h}.pqet")

    for h in avail_hashes:
        os.rename(f"{outdir}/labels/labels_{h}.pt", f"{outdir}/train/labels/labels_{h}.pt")
        os.rename(f"{outdir}/sets/data_{h}.pt", f"{outdir}/train/sets/data_{h}.pt")
        os.rename(f"{outdir}/meta/metadata_{h}.pqet", f"{outdir}/train/meta/metadata_{h}.pqet")

    if len(os.listdir(f"{outdir}/labels/")) > 0 or len(os.listdir(f"{outdir}/sets/")) > 0:
        raise ConfigError(f"Initial directories {outdir}/labels or {outdir}/sets has not been emptied as expected")
    os.removedirs(f"{outdir}/labels/")
    os.removedirs(f"{outdir}/sets/")
    os.removedirs(f"{outdir}/meta/")


def prepare_chunk(df, outdir, transform_function, reduce_function, balance_labels, sample_id, label_column="Good"):
    '''
    Takes the input DataFrame and transform it into a pytorch tensor saved in the output dir:
     - Applies a transform function (DataFrame -> DataFrame)
     - Applies a reduce function (DataFrame -> DataFrame, metadata)
     - Applies label balancing if requested
     - Computes object hash
     - Transforms labels and data in tensors and save them in the output (pt),
       save the metatada as pqet, create the columns.csv file (column to name mapping)
    '''
    dataset = transform_function(df)
    dataset, metadata = reduce_function(dataset)
    metadata["sample_id"] = sample_id

    if isinstance(label_column, list) and balance_labels:
        print("Labels are multiple - setting balance labels to False")
        balance_labels = False

    if balance_labels:
        # Only in the case of label_column not being a list
        n_good = sum(dataset[label_column] == True)
        n_bad = sum(dataset[label_column] == False)
        drop_indices = None
        if n_bad > n_good:
            bad_only = dataset.loc[dataset[label_column] == False]
            drop_indices = np.random.choice(bad_only.index, n_bad - n_good, replace = False)
        elif n_good > n_bad:
            good_only = dataset.loc[dataset[label_column] == True]
            drop_indices = np.random.choice(good_only.index, n_good - n_bad, replace = False)

        if drop_indices is None:
            sub_dataset = dataset
        else:
            sub_dataset = dataset.drop(drop_indices)
            metadata = metadata.drop(drop_indices)
    else:
        sub_dataset = dataset

    metadata = metadata.reset_index()

    if len(sub_dataset)==0:
        print(f"Warning: no samples found in the dataset")

    myhash = hashlib.sha256(pd.util.hash_pandas_object(sub_dataset, index = True).values).hexdigest()[:10]

    labels_df = sub_dataset[label_column]
    if isinstance(label_column, str):
        label_column = [label_column]
    data_df = sub_dataset.drop(columns = label_column)

    labels = torch.tensor(labels_df.values)
    data = torch.tensor(data_df.values.astype(np.float32))
    torch.save(labels, f"{outdir}/labels/labels_{myhash}.pt")
    torch.save(data, f"{outdir}/sets/data_{myhash}.pt")
    metadata.to_parquet(f"{outdir}/meta/metadata_{myhash}.pqet")
    if not os.path.exists(f"{outdir}/columns.csv"):
        sub_dataset.drop(sub_dataset.index).drop(columns = label_column).reset_index(drop = True).transpose().to_csv(f"{outdir}/columns.csv")

    return myhash, data_df, labels_df


def chunk_data(raw_file, outdir, chunk_size, sample_id, load_function, transform_function, reduce_function,
               max_chunk=None, balance_labels=True, frac_validate=0.1, frac_test=0., file_part_or_field="",
               return_sample_df=False, label_column="Good", **kwargs):
    '''
    Create the output directory structure and fill it with the pre-processed data, saved
    in pytorch tensor format.
     - Uses max_chunks or compute the number of chunks that are required based on the chunk_size.
     - load_function is the function used to create the DataFrame chunk
     - prepare_chunk is the function used to transform the DataFrame into a proper chunk. It returns the hash of the chunk.
     - It is completed by splitting the chunks into train/test/validate folders
    @param sample_id: written in the metadata
    @param raw_file: string path the the input raw file(s). Can be a glob expression
    @outdir: path to the output directory
    '''
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    if not os.path.exists(f"{outdir}/labels"):
        os.mkdir(f"{outdir}/labels")
    if not os.path.exists(f"{outdir}/sets"):
        os.mkdir(f"{outdir}/sets")
    if not os.path.exists(f"{outdir}/meta"):
        os.mkdir(f"{outdir}/meta")

    b_chunk = 0
    e_chunk = chunk_size
    chunks_sizes = io.get_last_entryIDs(raw_file, file_part_or_field)
    if chunks_sizes == [-1]:
        print("Cannot find last event in file")
        return None
    if max_chunk is None or max_chunk>sum(chunks_sizes):
        max_chunk = sum(chunks_sizes)

    sample_df = None
    list_hashes = []
    with tqdm(total = math.ceil(max_chunk / chunk_size)) as t:
        while b_chunk < max_chunk:
            df = load_function(raw_file, b_chunk, e_chunk, chunks_sizes)
            myhash, data, labels = prepare_chunk(df, outdir, transform_function, reduce_function, balance_labels, sample_id, label_column)
            list_hashes.append(myhash)
            if return_sample_df and sample_df is None:
                sample_df = data, labels
            b_chunk = e_chunk
            e_chunk += chunk_size
            t.update()

    print(f"Created {len(list_hashes)} hashes from of size {chunk_size}, of which {len(set(list_hashes))} unique")
    extract_train_validate_test(outdir, list_hashes, frac_validate, frac_test)
    return sample_df
