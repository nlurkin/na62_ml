import datetime
import os
import re
from pathlib import Path

import pandas as pd
import uproot


def find_files_with_regex(path, regex):
  p = Path(path)
  filelist = {}
  id_group = None
  other_group = None
  for a in p.glob(regex):
    m = re.search(regex.replace(".", "\.").replace("*", "(.*)"), str(a))
    if m:
      if id_group is None:
        for i, g in enumerate(m.groups()):
          try:
            int(g)
            id_group = i + 1
          except ValueError:
            other_group = i + 1
            continue
      if other_group is None and len(m.groups())>id_group:
        other_group = id_group + 1
      if other_group is not None:
        filelist[int(m.group(id_group))] = str(a).replace(m.group(other_group), "*")
      else:
        filelist[int(m.group(id_group))] = str(a)

  filelist = [filelist[_] for _ in sorted(filelist)]
  return filelist


def save_dataset(data_dir, fname, ext, dataset):
  ts = datetime.today().strftime("%y%m%d-%H%M")
  full_path = f"{data_dir}/{fname}_{ts}.{ext}"
  print(f"Saving dataset at {full_path}")
  dataset.to_parquet(full_path)


def load_dataset(filepath):
  print(f"Loading dataset at {filepath}")
  dataset = pd.read_parquet(filepath)
  return dataset


def load_or_generate(save_file, data_dir, func):
  print(f"Load or generate {save_file}")
  base, ext = save_file.split(".")
  save_present = sorted([file for file in os.listdir("data") if re.match(
      f"{base}_[0-9]+-[0-9]+\.{ext}", file) is not None])
  if len(save_present):
    print("Found saved pqet file")
    return load_dataset(f"{data_dir}/{save_present[-1]}")
  else:
    print("No pqet file found. Loading initial data")
    dataset = func()
    if dataset is not None:
      save_dataset(data_dir, base, ext, dataset)
    return dataset


def mkpath(path):
  if not os.path.exists(path):
    os.makedirs(path)

def blocks(files, size=65536):
    while True:
        b = files.read(size)
        if not b: break
        yield b


def get_file_last_entryID(file_path, file_part=None):
  if ".root" in file_path:
    fd = uproot.open(file_path)
    return fd[file_part].num_entries
  else:
    with open(file_path, "r", encoding="utf-8", errors='ignore') as f:
      return sum(bl.count("\n") for bl in blocks(f))-1 # Don't count the header line

def get_last_entryIDs(re_file_path, file_part_or_field):
  if "*" in re_file_path:
    p = Path(re_file_path)
    avail_files = find_files_with_regex(p.parent, p.name)
    return [get_file_last_entryID(_.replace("*", file_part_or_field), file_part=file_part_or_field) for _ in avail_files]
  else:
    return [get_file_last_entryID(re_file_path, file_part=file_part_or_field)]
