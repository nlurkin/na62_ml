import pandas as pd
from typing import List

def category_encoding(df: pd.DataFrame, col_names: List[str]):
  '''
  Apply category encoding on the passed dataframe to the specified columns
  '''
  type_dict = {}
  for col in col_names:
    if sum(df[col].isna()) > 0:
      df[col] = df[col].fillna("None")
    cats = pd.CategoricalDtype(
        list(df[col].value_counts().index), ordered=True)
    type_dict[col] = cats
  df = df.astype(type_dict)
  return df


def replace_nan_avg(df, col_names):
  '''
  For the specified columns, replace all NaNs in the dataframe by the column average
  '''
  for col in col_names:
    avg = df[col].mean()
    df[col] = df[col].fillna(avg)
  return df

