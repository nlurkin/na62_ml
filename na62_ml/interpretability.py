'''
Created on 9 Apr 2021

@author: Nicolas Lurkin
'''

import os

from captum.attr import IntegratedGradients
from captum.attr import LayerConductance
from captum.attr import NeuronConductance
from colorama.ansi import Fore
from scipy import stats
from sklearn.ensemble._forest import RandomForestClassifier
from sklearn.model_selection._validation import cross_val_score
import torch

import matplotlib.pyplot as plt
from na62_ml.ml_lib import DiskTensorDataset
import numpy as np
import pandas as pd


def split_dataset_by_labels(ds):
    adata = []
    alabels = []
    for d, l, _ in ds:
        adata.append(d.detach().cpu())
        alabels.append(l.detach().cpu())

    data, labels = torch.cat(adata, dim = 0), torch.cat(alabels, dim = 0)
    good = data.numpy()[labels == 1]
    bad = data.numpy()[labels == 0]

    return good, bad


def plot_features(dataloader, dataloader2 = None, save_file = False):
    if save_file and not "figures" in os.listdir():
        os.mkdir("figures")
    col_names = dataloader.dataset.get_columns_names()

    good, bad = split_dataset_by_labels(dataloader)

    if dataloader2 is not None:
        good2, bad2 = split_dataset_by_labels(dataloader2)

    if dataloader.dataset.with_transform is False:
        dlimits = {
            "gtk_Chi2": (0, 50),
            "ds_Chi2": (0, 50),
            "VtxX": (-200, 200),
            "VtxY": (-200, 200),
            "VtxZ": (-1, 1),
            "GTKX_": (-200, 100),
            "DSX_": (-100, 100),
            "DSY_": (-100, 100),
            "DS_": (-100, 100),
            "GTKSlopeY": (-0.0004, 0.0004),
            "GTKMagFit": (70000, 80000),
            "DSMagFit": (0, 80000),
            "CDA": (0, 50),
        }
        dbins = {
            "GTKSlopeY": 50,
            "gtk_SlopeY": 50,
        }
    else:
        dlimits = {}
        dbins = {}

    for icol, col in enumerate(col_names):
        if dataloader2 is None:
            fig = plt.figure()
            good_axis = fig.gca()
            bad_axis = fig.gca()
            bad_style = "step"
        else:
            fig, ax = plt.subplots(1, 2)
            good_axis = ax[0]
            bad_axis = ax[1]
            bad_style = "stepfilled"

        bins = 100
        limits = None
        if col in dlimits:
            limits = dlimits[col]
        if col in dbins:
            bins = dbins[col]

        good_axis.hist(good[:, icol], bins = bins, range = limits, density = True, label = "True")
        bad_axis.hist(bad[:, icol], bins = bins , range = limits, density = True, histtype = bad_style, label = "False")
        if dataloader2 is not None:
            good_axis.hist(good2[:, icol], bins = bins, range = limits, density = True, histtype = "step", label = "True2")
            bad_axis.hist(bad2[:, icol], bins = bins , range = limits, density = True, histtype = "step", label = "False2")

        fig.suptitle(col)
        good_axis.legend()
        bad_axis.legend()
        if save_file:
            fig.savefig(f"figures/{col}.png")


def get_interpretability_sample(dataloader, max_size):
    interpret_input_tensor = [[] for _ in range(2)]
    processed = 0
    with_transform = dataloader.dataset.with_transform
    dataloader.dataset.with_transform = True
    for data in dataloader:
        i = data[0].cpu()
        l = data[1].cpu()
        for ind in range(l.shape[0]):
            interpret_input_tensor[l[ind].item()].append(i[ind])

        processed += l.shape[0]
        if processed > max_size:
            break
    dataloader.dataset.with_transform = with_transform
    interpret_input_tensor[0] = torch.cat(interpret_input_tensor[0]).reshape(-1, dataloader.dataset[0][0].shape[0])
    interpret_input_tensor[1] = torch.cat(interpret_input_tensor[1]).reshape(-1, dataloader.dataset[0][0].shape[0])
    return interpret_input_tensor


def feature_importance(model, interpret_input_tensor, layer = None, neuron = None):
    if layer is None:
        test = IntegratedGradients(model)
    elif layer:
        if isinstance(layer, str):
            layer = getattr(model, layer)
        if neuron is None:
            test = LayerConductance(model, layer)
        else:
            test = NeuronConductance(model, layer)

    attr = []
    for i in range(len(interpret_input_tensor)):
        interpret_input_tensor[i].requires_grad_()
        if neuron is None:
            attribute = test.attribute(interpret_input_tensor[i], target = 1, baselines = torch.rand(interpret_input_tensor[i].shape)).detach().numpy()
        else:
            attribute = test.attribute(interpret_input_tensor[i], neuron_selector = neuron, target = 1).detach().numpy()
        attr.append(attribute)

    return attr


# Helper method to print importances and visualize distribution
def visualize_importances(feature_names, importances, title = "Average Feature Importances for positive output", plot = True, axis_title = "Features"):
    print(title)
    used_features = []
    used_importances = [[] for _ in range(2)]
    for i in range(len(feature_names)):
        if importances[0][i] == 0:
            continue
        print(feature_names[i], ": ", '%.3f' % (importances[1][i]))
        used_features.append(feature_names[i])
        used_importances[1].append(importances[1][i])
        used_importances[0].append(importances[0][i])
    x_pos = (np.arange(len(used_features))) * 2
    if plot:
        plt.figure(figsize = (12, 6))
        plt.bar(x_pos, used_importances[1], align = 'center', label = "True")
        plt.bar(x_pos + 0.8, used_importances[0], align = 'center', label = "False")
        plt.xticks(x_pos, used_features, wrap = True, rotation = 90, y = -0.1)
        plt.xlabel(axis_title)
        plt.title(title)
        plt.legend()
        plt.grid(linestyle = "dashed")
        # plt.tight_layout()


def visualize_importance_distributions(interpret_input_tensor, attributions, feature_names):
    for i in range(len(feature_names)):
        if np.mean(attributions[0], axis = 0)[i] == 0:
            continue
        fig, ax = plt.subplots(1, 2, figsize = (12, 6))
        ax[0].hist(attributions[1][:, i], 100, density = True);
        ax[0].hist(attributions[0][:, i], 100, histtype = "step", density = True);

        bin_means1, bin_edges1, _ = stats.binned_statistic(interpret_input_tensor[1].detach().numpy()[:, i], attributions[1][:, i], statistic = 'mean', bins = 6)
        bin_count1, _, _ = stats.binned_statistic(interpret_input_tensor[1].detach().numpy()[:, i], attributions[1][:, i], statistic = 'count', bins = 6)
        bin_width1 = (bin_edges1[1] - bin_edges1[0])
        bin_centers1 = bin_edges1[1:] - bin_width1 / 2

        bin_means0, bin_edges0, _ = stats.binned_statistic(interpret_input_tensor[0].detach().numpy()[:, i], attributions[0][:, i], statistic = 'mean', bins = 6)
        bin_count0, _, _ = stats.binned_statistic(interpret_input_tensor[0].detach().numpy()[:, i], attributions[0][:, i], statistic = 'count', bins = 6)
        bin_width0 = (bin_edges0[1] - bin_edges0[0])
        bin_centers0 = bin_edges0[1:] - bin_width0 / 2

        ax[1].scatter(bin_centers1, bin_means1, s = bin_count1)
        ax[1].scatter(bin_centers0, bin_means0, s = bin_count0, alpha = 0.5)
        ax[1].set_xlabel(f"Average {feature_names[i]} Feature Value");
        ax[1].set_ylabel("Average Attribution");

        fig.suptitle(f"Distribution of {feature_names[i]} Attribution Values");


def check_feature_shift(ds1_path, ds2_path, data_limit):
    # from na62_ml import wrapper
    # import torch
    # from sklearn.ensemble import RandomForestRegressor
    # from sklearn.ensemble import RandomForestClassifier
    # from sklearn.model_selection import cross_val_score
    # import numpy as np

    # Load datasets
    set1 = DiskTensorDataset(ds1_path, size_limit = data_limit)
    set2 = DiskTensorDataset(ds2_path, size_limit = data_limit)

    # Replace get_label function to get both the dataset id and the label
    set1.get_label = lambda x, v: (torch.tensor(0), x[v].long())
    set2.get_label = lambda x, v: (torch.tensor(1), x[v].long())

    # Assemble them in a single dataset
    cdl = torch.utils.data.ConcatDataset([set1, set2])

    # Create pandas dataframe out of the dataset
    all_features = []  # Features list
    all_dsid = []  # Dataset id list
    all_labels = []  # Labels list
    for f, l, _ in cdl:
        all_features.append(f.reshape(1, -1))
        all_dsid.append(l[0].reshape(1, -1))
        all_labels.append(l[1].reshape(1, -1))
    df = pd.DataFrame(torch.cat(all_features, 0).numpy(), columns = set1.get_columns_names())
    df["Labels"] = torch.cat(all_labels, 0).numpy()
    y = pd.Series(torch.cat(all_dsid, 0).reshape(-1).numpy())
    df["Dataset"] = y.values

    model = RandomForestClassifier(n_estimators = 50, max_depth = 5, min_samples_leaf = 5)
    print(f"  {'Feature':<20} | {'Full':>5.3} | {'Positive':>5.3} | {'Negative':>5.3}")
    for i in df.columns:
        score_all = cross_val_score(model, df[i].values.reshape(-1, 1), y, cv = 2, scoring = 'roc_auc')
        score_positive = cross_val_score(model, df.loc[df["Labels"] == 1][i].values.reshape(-1, 1), y.loc[df["Labels"] == 1], cv = 2, scoring = 'roc_auc')
        score_negative = cross_val_score(model, df.loc[df["Labels"] == 0][i].values.reshape(-1, 1), y.loc[df["Labels"] == 0], cv = 2, scoring = 'roc_auc')
        col = ""
        if (np.mean(score_all) > 0.8 or np.mean(score_positive) > 0.8 or np.mean(score_negative) > 0.8):
            col = Fore.RED
        print(col + f"  {i:<20} | {np.mean(score_all):>5.3} | {np.mean(score_positive):>5.3} | {np.mean(score_negative):>5.3}" + Fore.RESET)
