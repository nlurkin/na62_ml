#!/usr/bin/env python
# encoding: utf-8
'''
Prepare data from raw csv files to pytorch tensors

@author:     Nicolas Lurkin
@contact:    nicolas.lurkin@cern.ch
'''

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
import os
from pathlib import Path
import sys

import na62_ml.data as data
import na62_ml.matching as matching

__all__ = []
__version__ = 0.1
__date__ = '2021-02-19'
__updated__ = '2021-02-19'

def validate_args(args):
    p = Path(args.raw_file)
    nfiles = len(list(p.parent.glob(p.name)))
    if nfiles==0:
        raise Exception(f"No files found at path {args.raw_file}")

def run(args):
    p = Path(args.raw_file)
    nfiles = len(list(p.parent.glob(p.name)))
    print("-------")
    print(f"Preparing input raw data")
    print(f" - Input data: {args.raw_file} - Found {nfiles} files")
    print(f" - Output directory: {args.outdir}")
    print(f" - Prepare module: {args.which_prepare}")
    print(f" # Other parameters")
    print("\n".join([f"   - {val:>20} -> {getattr(args, val)}" for val in args.__dict__ if val not in ["raw_file", "outdir"]]))
    print("-------")
    print("Starting prepare")

    if args.which_prepare == "prepare":
        from na62_ml.matching import prepare as this_prepare
        data.dataset_create.chunk_data(load_function=matching.io.load_export_sf,
                                       transform_function=lambda x: this_prepare.transform_dataset(x, this_prepare.gtk_good_def_mc),
                                       reduce_function=lambda x: this_prepare.mom_reduce(x, 0, 100000, this_prepare.gtk_good_def_mc, True),
                                       file_part_or_field="evt", **vars(args))
    else:
        from na62_ml.matching import prepare_joel as this_prepare
        data.dataset_create.chunk_data(load_function=matching.io.load_root_files,
                                       transform_function= this_prepare.transform_dataset,
                                       reduce_function=this_prepare.reduce_dataset,
                                       file_part_or_field="KpiMatchInfo", **vars(args))


    print(f"Prepare complete")
    print()


def main(argv = None):  # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    program_name = os.path.basename(sys.argv[0])
    program_version = f"v{__version__}"
    program_build_date = str(__updated__)
    program_version_message = f"{program_name} {program_version} ({program_build_date})"
    program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
    help_desc = f"{program_version_message}\n{program_shortdesc}"

    # Setup argument parser
    parser = ArgumentParser(description = help_desc, formatter_class = RawDescriptionHelpFormatter)
    parser.add_argument("--raw_file", help = "Path to the raw data list (can be quoted-glob expression", required = True)
    parser.add_argument("--outdir", help = "Destination for the pre-processed data", required = True)
    parser.add_argument("--sample_id", help = "ID of the sample: written in the metadata", required = True)

    parser.add_argument("--chunk_size", default = 1000, help = "Size of the chunks [default: %(default)i]", type=int)
    parser.add_argument("--balance_labels", default = True, help = "Enforce good balance between labels [default: %(default)i]", type=bool)
    parser.add_argument("--max_chunk", help = "Max chunk size to process (max_chunk / chunk_size is the number of chunks that will be created)", type=int)
    parser.add_argument("--which_prepare", help = "Name of the python module providing the prepare functionality", default="prepare")

    # Process arguments
    args = parser.parse_args()

    validate_args(args)
    run(args)

    return 0


if __name__ == "__main__":
    sys.exit(main())
