#!/usr/bin/env python
# encoding: utf-8

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
import math
import os
from pathlib import Path
import shutil
import subprocess
import sys

htc_submit_file = """
requirements = (OpSysAndVer =?= "CentOS7")
executable              = submit.sh
arguments               = {executable_base} $Fnx(file) $(ProcId) \\"{options}\\" {eosoutdir} $(ClusterId)
output                  = output/$(ClusterId).$(ProcId).out
error                   = error/$(ClusterId).$(ProcId).err
log                     = log/$(ClusterId).log

+JobFlavour = "tomorrow"
should_transfer_files = YES
when_to_transfer_output = ON_EXIT_OR_EVICT
transfer_output_files   = ""
transfer_input_files = $(file),{executable}
queue file matching files inputs/input*.list
"""


def parse_list_file(path):
    with open(path) as fd:
        lines = list(map(lambda x: x.strip(), fd))
    return lines


def chunk_list(ifiles, nfiles):
    return [ifiles[n * nfiles:(n + 1) * nfiles] for n in range(math.ceil(len(ifiles) / nfiles))]

def prepare_submit_dir(sdir):
    if sdir.exists():
        answer = input(f"Submission directory {sdir} already exists. Are you sure you want to replace it? [Y/N]")
        if answer != "Y":
            return False
        shutil.rmtree(sdir)

    sdir.mkdir(parents = True)
    (sdir / "output").mkdir()
    (sdir / "error").mkdir()
    (sdir / "log").mkdir()
    (sdir / "inputs").mkdir()
    return True


def prepare_output_dir(sdir):
    print(" - Preparing output dir")
    if not sdir.exists():
        os.system(f"xrdfs xroot://eosna62.cern.ch// mkdir -p {sdir}")


def prepare_input_lists(chunked_input, sdir):
    print(" - Preparing input lists")
    for i, chunk in enumerate(chunked_input):
        with open(sdir / "inputs" / f"input.{i}.list", "w") as fd:
            fd.write("\n".join(chunk))
            fd.write("\n")


def prepare_submit_file(sdir, args):
    print(" - Preparing submit files")
    exe = Path(args.exe)
    odir = Path(args.odir)
    if not exe.exists():
        raise Exception(f"Executable {exe} does not exist")
    prepare_output_dir(odir)

    with open(sdir / "condor.sub", "w") as fd:
        fd.write(htc_submit_file.format(
            executable_base = exe.name,
            options = " ".join(args.options),
            eosoutdir = odir,
            executable = exe.absolute()))

    cwd = Path(__file__).parent.absolute()
    shutil.copy(cwd / "submit.sh", sdir / "submit.sh")
    return

def submit_to_condor(sdir):
    print(" - Submitting to condor")
    sub = subprocess.run(["condor_submit", "condor.sub"], stdout = subprocess.PIPE, text = True, cwd = sdir)
    print(sub.stdout)

    
def process(args):
    ifiles = parse_list_file(args.list)
    ndiv = math.floor(len(ifiles) / args.nfiles)
    njobs = math.ceil(len(ifiles) / args.nfiles)
    print("Number of files in list:", len(ifiles))
    print("Maximum number of files per job:", args.nfiles)
    print(f"Preparing to submit {njobs} jobs")
    if ndiv < njobs:
        print(f"Last job will contain only {len(ifiles)-ndiv*args.nfiles} input files")
    
    sdir = Path(args.subdir)
    if not prepare_submit_dir(sdir):
        return


    chunked_input = chunk_list(ifiles, args.nfiles)
    prepare_input_lists(chunked_input, sdir)

    prepare_submit_file(sdir, args)

    submit_to_condor(sdir)


def main(argv = None):  # IGNORE:C0111
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    parser = ArgumentParser(description = "", formatter_class = RawDescriptionHelpFormatter)
    parser.add_argument("--list", help = "Path to the input file list", required = True)
    parser.add_argument("--nfiles", help = "Maximum number of files per job", required = True, type = int)
    parser.add_argument("--subdir", help = "Path to the submission directory", default = "./condor_subdir")
    parser.add_argument("--exe", help = "Path to the executable", required = True)
    parser.add_argument("--odir", help = "Path to the EOS output directory", required = True)
    parser.add_argument('options', nargs = "*")

    # Process arguments
    args = parser.parse_args()
    print(args)

    process(args)
    return 0


if __name__ == "__main__":
    sys.exit(main())
