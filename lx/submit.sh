#!/bin/sh

EXE=$1
INPUTLIST=$2
JOBNO=$3
OPTIONS=$4
EOSOUT=$5
CLUSTERNO=$6

# Copy input list locally
mkdir input
for f in `cat ${INPUTLIST}`; do
  xrdcp xroot://eosna62.cern.ch//${f} input/
done

ls input/* > local_input.list
if [ `cat ${INPUTLIST} | wc -l` != `cat local_input.list | wc -l` ]; then
  echo "Inconsistent input lists"
  return 1
fi

source /afs/cern.ch/work/n/nlurkin/na62ml/scripts/env.sh
./${EXE} -l local_input.list ${OPTIONS}

ret=$?
if [ ${ret} != 0 ]; then
  exit ${ret}
fi

ls *.csv | xargs -I {} mv {} {}.${JOBNO}
ls *.root | xargs -I {} mv {} {}.${JOBNO}

JOBID="${CLUSTERNO}.${JOBNO}"
tar -zcvf ${JOBID}.tgz *.csv.${JOBNO} *.root.${JOBNO}

xrdcp ${JOBID}.tgz xroot://eosna62.cern.ch//${EOSOUT}/
