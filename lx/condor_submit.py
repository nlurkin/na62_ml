#!/usr/bin/env python3
# encoding: utf-8
'''
LXplus HTCondor submission wrapper

@author:     Nicolas Lurkin
@contact:    nicolas.lurkin@cern.ch
'''

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
import sys
import os
from pathlib import Path
import tarfile
from XRootD import client
from XRootD.client.flags import DirListFlags, MkDirFlags
import subprocess
import json

status_map = {1: "IDLE", 2: "RUNNING", 3: "REMOVED", 4: "COMPLETED", 5: "HELD", 6: "TRANSFERRING_OUTPUT", 7: "SUSPENDED"}
wrapper_template = '''#!/bin/bash

source /afs/cern.ch/user/n/nlurkin/.bashrc
conda activate ml15

eos_source={eos_source}
./{submit_script} job --name {job_name} --logdir {logdir}

./{executable} --data data/{sample} --config NNConfigs/{nn_config} --logdir {logdir} --num-workers=2 {additional_params}

./{submit_script} finish --name {job_name} --logdir {logdir}
'''

conda_submit = '''
executable              = {wrapper_file}
arguments               = $(ClusterId).$(ProcId)
transfer_input_files    = {submit_script},{wrapper_file},{executable}
should_transfer_files   = YES
when_to_transfer_output = ON_EXIT_OR_EVICT
transfer_output_files   = {logdir}
output                  = out.$(ClusterId).$(ProcId)
error                   = err.$(ClusterId).$(ProcId)
log                     = log.$(ClusterId).$(ProcId)
request_cpus            = 2
+JobFlavour = "testmatch"
+Requirements           = OpSysAndVer =?= "CentOS7"
queue 1
'''

eos_mgm_url = "xroot://eosna62.cern.ch/"
eos_main_path = "/eos/experiment/na62/user/n/nlurkin/"
xroot_client = None

'''
Utility functions
'''


def archive(archive_name, path, outdir):
  print(f"# Archiving {path} into {archive_name} with name {outdir}")
  with tarfile.open(archive_name, "w:gz") as tar:
    tar.add(path, arcname=f"{outdir}/{path.name}")


def extract(archive_name):
  print(f"# Extracting archive {archive_name}")
  with tarfile.open(archive_name, "r:gz") as tar:
    tar.extractall()


def to_local_path(path):
  return str(path.absolute())


def to_eos_path(path):
  return f"{eos_mgm_url}{path}"


def eos_copy(ipath, opath, to_eos=True):
  print(f"# Copying {ipath} to {opath}")
  myclient = get_xroot_client()

  if to_eos:
    ipath_str = to_local_path(ipath)
    opath_str = to_eos_path(opath)
  else:
    ipath_str = to_eos_path(ipath)
    opath_str = to_local_path(opath)

  status, _ = myclient.copy(ipath_str, opath_str, force=True)
  if not status.ok:
    raise IOError(
        f"Unable to copy to eos: {ipath} -> {opath}\n{status.message}")


def write_wrapper():
  pass


def get_xroot_client():
  global xroot_client
  if xroot_client is None:
    xroot_client = client.FileSystem(eos_mgm_url)
  return xroot_client


'''
Main functions
'''


def prepare(args, add_args):
  eos_dir = f"{eos_main_path}/torch_outputs/{args.name}"
  myclient = get_xroot_client()

  present = []
  if args.skip_exist:
    status, listing = myclient.dirlist(eos_dir, DirListFlags.STAT)
    if status.ok:
      for l in listing:
        present.append(l.name)

  # Create archives: na62_ml, data,config,logdir
  tempPath = Path("./temp_dir")
  tempPath.mkdir(exist_ok=True)

  archive_config = (
      ("na62_ml_lib.tgz", Path("na62_ml"), "", True),
      ("data.tgz", args.data, "data", True),
      ("nnconfig.tgz", args.config, "NNConfigs", True),
      ("logdir.tgz", args.logdir, "", False)
  )

  for arch_name, ipath, oname, required in archive_config:
    if not ipath.exists():
      if required:
        raise IOError(f"Expected directory {ipath}")
      else:
        continue
    if arch_name not in present:
      archive(tempPath / arch_name, ipath, oname)

  # create a dedicated job directory on eos
  myclient.mkdir(eos_dir, MkDirFlags.MAKEPATH)

  # Copy all archives to job directory
  for arch_name, ipath, oname, required in archive_config:
    if arch_name not in present and (tempPath/arch_name).exists():
      eos_copy(tempPath / arch_name, f"{eos_dir}/{arch_name}")

  # Create a local job directory
  local_dir = Path(f"./jobs_dir/{args.name}")
  local_dir.mkdir(parents=True, exist_ok=True)

  # Create the run script
  with open(local_dir / "wrapper.sh", "w") as fd:
    fd.write(wrapper_template.format(
        eos_source=eos_dir,
        submit_script=Path(__file__).name,
        executable="train_model.py",
        sample=args.data.name,
        nn_config=args.config.name,
        logdir=args.logdir,
        additional_params=" ".join(add_args),
        job_name=args.name,
    ))
  # Create the submission file ?
  submission_file = conda_submit.format(
        wrapper_file=(local_dir / "wrapper.sh").absolute(),
        submit_script=Path(__file__).absolute(),
        executable=Path("train_model.py").absolute(),
        logdir=args.logdir,
    )
  with open(local_dir/"condor.submit", "w") as fd:
     fd.write(submission_file)

  # Do the submission
  process = subprocess.Popen(["condor_submit", "-"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, cwd=local_dir)
  out, errs = process.communicate(input=submission_file.encode("utf8"))
  retcode = process.wait()
  if retcode != 0:
    raise OSError(f"condor_submit failed\n{errs}")

  parameters = [_.split("=") for _ in out.decode("utf8").split("\n") if len(_.split("="))==2]
  parameters = {k.strip(): v.strip()for k,v in parameters}

  with open(local_dir / "job_id.json", "w") as fd:
    json.dump(parameters, fd)


def job(args, _):
  # Download everything from the remote eos
  eos_dir = f"{eos_main_path}/torch_outputs/{args.name}"
  myclient = get_xroot_client()

  status, listing = myclient.dirlist(eos_dir, DirListFlags.STAT)
  found = []
  if status.ok:
    for l in listing:
      if "tgz" in l.name and not "checkpoints" in l.name:
        eos_copy(Path(f"{eos_dir}/{l.name}"), Path(l.name), False)
        found.append(l.name)

  # Extract everything
  for archive in found:
    extract(archive)

  if not args.logdir.exists():
    args.logdir.mkdir(parents=True)


def finish(args, _):
  eos_dir = f"{eos_main_path}/torch_outputs/{args.name}"
  checkpoint_file = f"checkpoints.{args.name}.tgz"
  archive(checkpoint_file, args.logdir, "")
  eos_copy(Path(checkpoint_file), Path(f"{eos_dir}/{checkpoint_file}"))


def check(args, _):
  # extract job ID
  local_dir = Path(f"./jobs_dir/{args.name}")

  with open(local_dir / "job_id.json", "r") as fd:
    parameters = json.load(fd)

  process = subprocess.Popen(["condor_q", f"{parameters['ClusterId']}.{parameters['ProcId']}", "-json"], stdout=subprocess.PIPE)
  out, errs = process.communicate()
  retcode = process.wait()
  if retcode != 0:
    raise OSError(f"condor_q failed\n{errs}")

  if len(out)==0:
    # no more info from condor_q -> move to condor_history
    process = subprocess.Popen(["condor_history", f"{parameters['ClusterId']}.{parameters['ProcId']}", "-json"], stdout=subprocess.PIPE)
    out, errs = process.communicate()
    retcode = process.wait()
    if retcode != 0:
      raise OSError(f"condor_history failed\n{errs}")

  out = out.decode("utf8").strip("\n ")
  output_dict = json.loads(out)
  if len(output_dict)>0:
    status = output_dict[0]["JobStatus"]
  else:
    print("Could not be found in history -- assuming completed successfully")
    status = 4
  if status != 4:
    print(f"Job is currently in state: {status_map[status]}")
    return

  # Download and extract checkpoint file
  eos_dir = Path(f"{eos_main_path}/torch_outputs/{args.name}")

  archive_name = f"checkpoints.{args.name}.tgz"
  eos_copy(eos_dir/archive_name, Path("temp_dir")/archive_name, to_eos=False)
  extract(f"temp_dir/{archive_name}")




def main(argv=None):  # IGNORE:C0111
  '''Command line options.'''

  if argv is None:
    argv = sys.argv
  else:
    sys.argv.extend(argv)

  program_name = os.path.basename(sys.argv[0])
  help_desc = __import__('__main__').__doc__.split("\n")[1]

  # Setup argument parser
  parser = ArgumentParser(description=help_desc,
                          formatter_class=RawDescriptionHelpFormatter)
  common_parser = ArgumentParser()
  common_parser.add_argument("--name", help="Job name", required=True)
  subparsers = parser.add_subparsers(dest="sub-command", help='sub-command help',
                                     required=True)

  parser_prepare = subparsers.add_parser('submit', help='prepare and create the HTCondor submission',
                                         parents=[common_parser], add_help=False)
  parser_prepare.add_argument("--data", help="Path to the on-disk data",
                              required=True, type=Path)
  parser_prepare.add_argument("--config", help="Path to config file",
                              required=True, type=Path)
  parser_prepare.add_argument("--logdir", default="runs",
                              help="Path to logdir [default: %(default)s]", type=Path)
  parser_prepare.add_argument("--skip-exist", action="store_true",
                              help="Skip creating archives if already exist")
  parser_prepare.set_defaults(func=prepare)

  parser_job = subparsers.add_parser('job', help='run the job side of preparation',
                                     parents=[common_parser], add_help=False)
  parser_job.add_argument("--logdir", default="runs",
                              help="Path to logdir [default: %(default)s]", type=Path)
  parser_job.set_defaults(func=job)
  parser_finish = subparsers.add_parser('finish', help='complete the job before returning',
                                     parents=[common_parser], add_help=False)
  parser_finish.add_argument("--logdir", default="runs",
                              help="Path to logdir [default: %(default)s]", type=Path)
  parser_finish.set_defaults(func=finish)
  parser_check = subparsers.add_parser('check', help='check the HTCondor job status and finalise if completed',
                                       parents=[common_parser], add_help=False)
  parser_check.set_defaults(func=check)

  # Process arguments
  args, pass_args = parser.parse_known_args()
  print(args, pass_args)

  args.func(args, pass_args)

  return 0


if __name__ == "__main__":
  sys.exit(main())
