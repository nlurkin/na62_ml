#!/bin/bash

source /afs/cern.ch/user/n/nlurkin/.bashrc
conda activate ml15

cp -r /afs/cern.ch/user/n/nlurkin/work/na62_ml/na62_ml .
# ./train_model.py --data data/mc_pnn_overlay_smallstats/v3/MCMatch_BT_Balanced/ --config NNConfigs/config_pnn/config.py --logdir logs_nicolas/ --nepochs 40 --num-workers 2 --pre /afs/cern.ch/user/n/nlurkin/work/na62_ml/scripts/download.py --pre-args xroot://eosna62.cern.ch//eos/experiment/na62/user/n/nlurkin/NNConfigs.tgz xroot://eosna62.cern.ch//eos/experiment/na62/user/n/nlurkin/testJoel.tgz xroot://eosna62.cern.ch//eos/experiment/na62/user/n/nlurkin/data.tgz
./train_model.py --data data/mc_pnn_overlay/v3/MCMatch_BT_Balanced/ --config NNConfigs/config_pnn/config.py --logdir logs_nicolas/ --nepochs 10 --num-workers 2 --pre /afs/cern.ch/user/n/nlurkin/work/na62_ml/scripts/download.py --pre-args xroot://eosna62.cern.ch//eos/experiment/na62/user/n/nlurkin/NNConfigs.tgz xroot://eosna62.cern.ch//eos/experiment/na62/user/n/nlurkin/data_nicolas.tgz

checkpoint_file=checkpoints_${1}.tgz
tar -zcvf ${checkpoint_file} logs_nicolas
xrdcp ${checkpoint_file} xroot://eosna62.cern.ch//eos/experiment/na62/user/n/nlurkin/torch_outputs/${checkpoint_file}
