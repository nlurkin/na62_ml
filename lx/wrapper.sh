#!/bin/bash

source /afs/cern.ch/user/n/nlurkin/.bashrc
conda activate ml15

cp -r /afs/cern.ch/user/n/nlurkin/work/na62_ml/na62_ml .
./train_model.py --data data/joelIndex/v5_high_intensity/ --config NNConfigs/config_pnn/config_joelnet.py --logdir testJoel/ --nepochs 40 --num-workers 2 --pre /afs/cern.ch/user/n/nlurkin/work/na62_ml/scripts/download.py --pre-args xroot://eosna62.cern.ch//eos/experiment/na62/user/n/nlurkin/NNConfigs.tgz xroot://eosna62.cern.ch//eos/experiment/na62/user/n/nlurkin/testJoel.tgz xroot://eosna62.cern.ch//eos/experiment/na62/user/n/nlurkin/data.tgz

checkpoint_file=checkpoints_${1}.tgz
tar -zcvf ${checkpoint_file} testJoel
xrdcp ${checkpoint_file} xroot://eosna62.cern.ch//eos/experiment/na62/user/n/nlurkin/torch_outputs/${checkpoint_file}
