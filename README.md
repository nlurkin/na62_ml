# NA62ML repo

## Directories
  * raw_data: Contains raw data exports from NA62Analysis
  * data: Contains transformed data in the form of datasets ready for fast import into ML
  * na62_ml: Main python package
  * NNConfigs: Contains various NN definitions used for training
  * tsboard: Contains the TensorBoard runs and the NN checkpoints

# Analysis steps
  * Create raw dataset (provided by PNN group, or from Analyzer)
  * Prepare dataset
    * For my dataset (CSV)
    ```
    ./prepare_data.py --raw_file "/home/nlurkin/jupyter/PyTorch/raw_data/mc_pnn_overlay/v3_small_stat_bckp/export.*.csv.*" --outdir data/v3/testme --sample_id "testme" --chunk_size=1000 --balance_labels=<0|1>
    ```
    * For Joel's dataset (root)
    ```
    ./prepare_data.py --raw_file "/home/nlurkin/jupyter/PyTorch/K3piMC_JJoel.root" --outdir data/joel/vtest --sample_id "TestJoel" --chunk_size=1000 --balance_labels=<0|1> --which_prepare=prepare_joel
    ```

  * Training
    * In jupyter, using `gtk_ds_matching.ipynb` or `gtk_ds_matching_lightning.ipynb` or `joel.ipynb`
    * With the script
    ```
    ./train_model.py --data data/mc_pnn_overlay/v3/MCMatch_BT_Balanced/ --nepochs 50 --config NNConfigs/config_pnn/config.py
                      --logdir tsboard/v3/mc_pnn_overlay/AllNoMom/ [--resume-from RESUME_FROM]
                      [--resume-epoch RESUME_EPOCH] [--data-limit DATA_LIMIT]
                      --num-workers 8

    ```
    or
    ```
    ./train_model.py --data data/joel/v8 --nepochs 50 --config NNConfigs/config_pnn/config.py
                      --logdir testJoel [--resume-from RESUME_FROM]
                      [--resume-epoch RESUME_EPOCH] [--data-limit DATA_LIMIT]
                      --num-workers 8

    ```