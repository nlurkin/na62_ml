#include <torch/script.h>
#include <iostream>
#include <boost/tokenizer.hpp>

torch::jit::script::Module fModule;

bool init_torch(std::string model_file) {
  std::cout << "Loading model: " << model_file << "\n";

  /* Note: I don't catch the exception here (const c10::Error& e),
       -> Fail if we cannot load the model file.
       */

  try {
    fModule = torch::jit::load(model_file);
  } catch(const c10::Error &exception) {
    std::cout << "Error loading the " << model_file << " model\n";
    return false;
  }

  /* !!! INOP if eval is not called before tracing the model !!! */
  /* Change the behaviour of the dropout and batch norm layers */
  /* See https://pytorch.org/docs/master/nn.html#torch.nn.BatchNorm1d
     *     https://pytorch.org/docs/master/nn.html#dropout
     * */
  fModule.eval();

  /* Disable the autograd as we are not doing any backprop. */
  torch::NoGradGuard no_grad;

  return true;
}

std::vector<at::Tensor> read_csv(std::string filePath) {
  std::cout << "Loading input file: " << filePath << std::endl;
  std::vector<at::Tensor> inputs;

  std::ifstream is(filePath);
  std::string line;
  bool header = true;
  while(std::getline(is,line)){
    if(header) {
      header = false;
      continue;
    }
    std::vector<float> values;
    boost::tokenizer<boost::escaped_list_separator<char> > tk(line, boost::escaped_list_separator<char>('\\', ',', '\"'));
    bool index(true);
    for (boost::tokenizer<boost::escaped_list_separator<char> >::iterator i(tk.begin()); i!=tk.end();++i) {
      if(index){
        index = false;
        continue;
      }
      values.push_back(std::stof(*i));
    }
    int64_t vec_size = values.size();
    inputs.push_back(torch::from_blob(values.data(), {1, vec_size}).clone());
  }
  return inputs;
}

bool test_torch_model(std::string ipath) {
  std::vector<at::Tensor> inputs = read_csv(ipath + "/input_norm.csv");
  std::vector<at::Tensor> py_outputs = read_csv(ipath + "/output.csv");
  std::vector<at::Tensor> labels = read_csv(ipath + "/labels.csv");

  int equal=0;
  int correct=0;
  for(uint iInput=0; iInput<inputs.size(); ++iInput){
    at::Tensor output = fModule.forward(std::vector<torch::jit::IValue>({inputs[iInput]})).toTensor();
    if(output.allclose(py_outputs[iInput])) ++equal;
    bool output_val = torch::argmax(output).item<bool>();
    if(output_val == labels[iInput][0].item<bool>()) ++correct;
  }

  uint total = inputs.size();
  std::cout << " --- MODEL TEST RESULTS ---" << std::endl;
  std::cout << " # Tested " << total << " inputs" << std::endl;
  std::cout << " # Compatible outputs: " << equal << " (" << ((float)equal/total)*100 << "%)" << std::endl;
  std::cout << " # Accurate   outputs: " << correct  << " (" << ((float)correct/total)*100 << "%)" << std::endl;
  std::cout << " --- END ---" << std::endl;
  return correct==equal;
}

bool test_normalization(std::string ipath) {
  std::vector<at::Tensor> inputs = read_csv(ipath + "/input_norm.csv");
  std::vector<at::Tensor> inputs_raw = read_csv(ipath + "/input_raw.csv");
  std::vector<at::Tensor> mu_v = read_csv(ipath + "/mu.csv");
  std::vector<at::Tensor> sigma_v = read_csv(ipath + "/sigma.csv");

  at::Tensor mu = mu_v[0];
  at::Tensor sigma = sigma_v[0];

  int correct = 0;
  for(uint iInput=0; iInput<inputs.size(); ++iInput){
    at::Tensor normalized_raw = (inputs_raw[iInput] - mu) / sigma;
    if(normalized_raw.allclose(inputs[iInput])) ++correct;
  }

  uint total = inputs.size();
  std::cout << " --- TESTING NORMALIZATION ---" << std::endl;
  std::cout << " # Tested " << total << " inputs" << std::endl;
  std::cout << " # Compatible normalization: " << correct << " (" << ((float)correct/total)*100 << "%)" << std::endl;
  std::cout << " --- END ---" << std::endl;

  return true;
}

int main(int argc, char **argv) {
  init_torch("../model.pt");

  test_torch_model("..");
  test_normalization("..");
}