#!/usr/bin/env python

import os
import sys
from pathlib import Path

def transfer(path):
  os.system(f"xrdcp {path} .")
  os.system(f"tar -zxf {Path(path).name}")

if __name__=="__main__":
  download_paths = sys.argv[1:]
  for path in download_paths:
    transfer(path)