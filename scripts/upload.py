#!/usr/bin/env python

import os
import sys
from pathlib import Path

def transfer(path):
  local_path = f"{Path(path).name}.tgz"
  os.system(f"tar -zcf {local_path}")
  os.system(f"xrdcp {local_path} {path}")

if __name__=="__main__":
  download_paths = sys.argv[1:]
  for path in download_paths:
    transfer(path)